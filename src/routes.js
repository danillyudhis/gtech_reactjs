/*!

=========================================================
* Argon Dashboard React - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

/*
import Index from "views/Index.js";
import Profile from "views/examples/Profile.js";
import Maps from "views/examples/Maps.js";
import Register from "views/examples/Register.js";
import Login from "views/examples/Login.js";
import Tables from "views/examples/Tables.js";
import Icons from "views/examples/Icons.js";
*/

import Dashboard from "views/Dashboard/Dashboard.js";
import Pesan from "views/Pesan/Pesan.js";
import Users from "views/Users/Users.js";
import Pendaftaran from "views/Pendaftaran/Pendaftaran.js";
import Rutinitas from "views/Rutinitas_kls/Rutinitas";
import Akademik from "views/Akademik/Akademik.js";
import Kehadiran from "views/Kehadiran";
import Tingkah_lk from "views/Tingkah_lk";
import Berita from "views/Berita";
import Ruang_kelas from "views/Ruang_kelas/Ruang_kelas";
import Akunting from "views/Akunting/Akunting.js";
import General_report from "views/General_report";
import Academic_setting from "views/Academic_setting";
import Settings from "views/Settings";
import Pemberitahuan from "views/Pemberitahuan/Index";
/*
const routes = [
  {
    path: "/index",
    name: "Dashboard",
    icon: "ni ni-tv-2 text-primary",
    component: Index,
    layout: "/admin"
  },
  {
    path: "/icons",
    name: "Icons",
    icon: "ni ni-planet text-blue",
    component: Icons,
    layout: "/admin"
  },
  {
    path: "/maps",
    name: "Maps",
    icon: "ni ni-pin-3 text-orange",
    component: Maps,
    layout: "/admin"
  },
  {
    path: "/user-profile",
    name: "User Profile",
    icon: "ni ni-single-02 text-yellow",
    component: Profile,
    layout: "/admin"
  },
  {
    path: "/tables",
    name: "Tables",
    icon: "ni ni-bullet-list-67 text-red",
    component: Tables,
    layout: "/admin"
  },
  {
    path: "/login",
    name: "Login",
    icon: "ni ni-key-25 text-info",
    component: Login,
    layout: "/auth"
  },
  {
    path: "/register",
    name: "Register",
    icon: "ni ni-circle-08 text-pink",
    component: Register,
    layout: "/auth"
  },
] */

const routes = [
  {
    path: "/dashboard_v2",
    name: "Dashboard",
    icon: "ni ni-tv-2",
    component: Dashboard,
    layout: "/user"
  },
  {
    path: "/pesan",
    name: "Pesan",
    icon: "far fa-envelope",
    component: Pesan,
    layout: "/user"
  },
  {
    path: "/users",
    name: "Users",
    icon: "fas fa-user-friends",
    component: Users,
    layout: "/user"
  },
  {
    path: "/pendaftaran",
    name: "Pendaftaran",
    icon: "fas fa-university",
    component: Pendaftaran,
    layout: "/user"
  },
  {
    path: "/rutinitas_kelas",
    name: "Rutinitas Kelas",
    icon: "far fa-clock",
    component: Rutinitas,
    layout: "/user"
  },
  {
    path: "/akademik",
    name: "Akademik",
    icon: "fas fa-pencil-alt",
    component: Akademik,
    layout: "/user"
  },
  {
    path: "/perpustakaan",
    name: "Perpustakaan",
    icon: "fas fa-book",
    component: Dashboard,
    layout: "/user"
  },
  {
    path: "/kehadiran",
    name: "Kehadiran",
    icon: "fas fa-calendar-week",
    component: Kehadiran,
    layout: "/user"
  },
  {
    path: "/kalender",
    name: "Kalender",
    icon: "far fa-calendar-alt",
    component: Dashboard,
    layout: "/user"
  },
  {
    path: "/files",
    name: "Files",
    icon: "far fa-folder",
    component: Dashboard,
    layout: "/user"
  },
  {
    path: "/polls",
    name: "Polls",
    icon: "fas fa-chart-pie",
    component: Dashboard,
    layout: "/user"
  },
  {
    path: "/pemberitahuan",
    name: "Pemberitahuan",
    icon: "far fa-bell",
    component: Pemberitahuan,
    layout: "/user"
  },
  {
    path: "/tingkah_laku",
    name: "Tingkah Laku",
    icon: "fas fa-gavel",
    component: Tingkah_lk,
    layout: "/user"
  },
  {
    path: "/berita",
    name: "berita",
    icon: "far fa-newspaper",
    component: Berita,
    layout: "/user"
  },
  {
    path: "/school_bus",
    name: "School Bus",
    icon: "fas fa-bus",
    component: Dashboard,
    layout: "/user"
  },
  {
    path: "/ruang_kelas",
    name: "Ruang Kelas",
    icon: "fas fa-school",
    component: Ruang_kelas,
    layout: "/user"
  },
  {
    path: "/akunting",
    name: "Akunting",
    icon: "fas fa-calculator",
    component: Akunting,
    layout: "/user"
  },
  {
    path: "/system_report",
    name: "System Report",
    icon: "far fa-file-alt",
    component: General_report,
    layout: "/user"
  },
  {
    path: "/pengaturan_akademik",
    name: "Pengaturan Akademik",
    icon: "fas fa-book-open",
    component: Academic_setting,
    layout: "/user"
  },
  {
    path: "/settings",
    name: "Settings",
    icon: "fas fa-cog",
    component: Settings,
    layout: "/user"
  },
];
export default routes;
