import React, { Component } from 'react'
import { BoxLoading } from 'react-loadingg';

class Load extends Component {
    render() {
        return(
            <>
            <div className="d-flex justify-content-center">
                 <BoxLoading/>
            </div>
            </>
        );
    }
}

export default Load;