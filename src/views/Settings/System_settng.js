import React, { Component } from 'react';
import { Input , Row, Col , Label, Card, Container , Button, FormGroup } from 'reactstrap';

class System_settng extends Component {
    render() {
        return(
            <>
            <Row className="mt-2 mb-2">
                <Col xl="12" className="mt-2 mb-2">
                    <Card className="shadow">
                        <Container>
                            <h1>System Settings</h1>
                            <Row>
                                <Col xl="6">
                                    <FormGroup>
                                        <Label>System Name</Label>
                                        <Input type="text" name="email" placeholder="with a placeholder" />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>System Email</Label>
                                        <Input type="email" name="email" placeholder="with a placeholder" />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Alamat</Label>
                                        <Input type="textarea" name="text" />
                                    </FormGroup>
                                </Col>
                                <Col xl="6">
                                   <FormGroup>
                                        <Label>System Title</Label>
                                        <Input type="text" name="email" placeholder="with a placeholder" />
                                    </FormGroup>
                                   <FormGroup>
                                        <Label>System Phone</Label>
                                        <Input type="number" name="email" placeholder="with a placeholder" />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col xl="4">
                                    <Label>Language</Label>
                                    <Input type="select" name="select" id="exampleSelect">
                                    <option>Pilih</option>
                                    <option>Indonesia</option>
                                    <option>Inggris</option>
                                    <option>Jepang</option>
                                    <option>France</option>
                                    </Input>
                                </Col>
                                <Col xl="4">
                                    <Label>Timezone</Label>
                                    <Input type="select" name="select" id="exampleSelect">
                                    <option>Pilih</option>
                                    <option>UTC/GMT +00:00 - Africa/Abidjan</option>
                                    <option>UTC/GMT +00:00 - Africa/Accra</option>
                                    <option>UTC/GMT +03:00 - Africa/Addis_Ababa</option>
                                    </Input>
                                </Col>
                                <Col xl="4">
                                    <Label>Running year</Label>
                                    <Input type="select" name="select" id="exampleSelect">
                                    <option>Pilih</option>
                                    <option>2020</option>
                                    <option>2021</option>
                                    <option>2022</option>
                                    </Input>
                                </Col>
                            </Row>
                            <Row className="mt-2">
                                <Col xl="6">
                                    <FormGroup>
                                        <Label>Currency</Label>
                                        <Input type="number" name="email" placeholder="with a placeholder" />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Facebook</Label>
                                        <Input type="text" name="email" placeholder="with a placeholder" />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Instagram</Label>
                                        <Input type="text" name="email" placeholder="with a placeholder" />
                                    </FormGroup>
                                </Col>
                                <Col xl="6">
                                    <FormGroup>
                                        <Label>Paypal Email</Label>
                                        <Input type="email" name="email" placeholder="with a placeholder" />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Twitter</Label>
                                        <Input type="text" name="email" placeholder="with a placeholder" />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label>Youtube</Label>
                                        <Input type="text" name="email" placeholder="with a placeholder" />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Button color="info" className="m-2">Update</Button>
                        </Container>
                    </Card>
                </Col>
                <Col xl="12" className="mb-2 mt-2">
                    <Card className="shadow">
                        <Container>
                            <h1>Social login</h1>
                            <FormGroup>
                                <Label>Facebook Sync UR</Label>
                                <Input type="text" name="email" placeholder="with a placeholder" />
                            </FormGroup>
                            <FormGroup>
                                <Label>Facebook Login UR</Label>
                                <Input type="text" name="email" placeholder="with a placeholder" />
                            </FormGroup>
                            <FormGroup>
                                <Label>Google Sync UR</Label>
                                <Input type="text" name="email" placeholder="with a placeholder" />
                            </FormGroup>
                            <FormGroup>
                                <Label>Google Login UR</Label>
                                <Input type="text" name="email" placeholder="with a placeholder" />
                            </FormGroup>
                            <Button color="info" className="m-2">Update</Button>
                        </Container>
                    </Card>
                </Col>
                <Col xl="12" className="mb-2 mt-2">
                    <Card className="shadow">
                        <Container>
                            <h1>Personalization</h1>
                            <Row>
                                <Col xl="6">
                                    <FormGroup>
                                        <Label for="exampleFile">Logo Type</Label>
                                        <Input type="file" name="file" id="exampleFile" />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="exampleFile">Icon White</Label>
                                        <Input type="file" name="file" id="exampleFile" />
                                    </FormGroup>
                                </Col>
                                <Col xl="6">
                                    <FormGroup>
                                        <Label for="exampleFile">Logo White</Label>
                                        <Input type="file" name="file" id="exampleFile" />
                                    </FormGroup>
                                    <FormGroup>
                                        <Label for="exampleFile">Favicon</Label>
                                        <Input type="file" name="file" id="exampleFile" />
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Button color="info" className="m-2">Update</Button>
                        </Container>
                    </Card>
                </Col>
            </Row>
            </>
        );
    }
}

export default System_settng;