import React, { Component } from 'react';
import { Button, FormGroup, Label, Input, Row, Col, Card , Container } from 'reactstrap';

class Sms extends Component {
    render() {
        return(
            <>
            <FormGroup>
                <Label>Sms Service</Label>
                <Input type="select" name="selectMulti" >
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                </Input>
            </FormGroup>
            <Row className="mt-2">
                <Col xl="4">
                    <Card className="shadow mb-1 mt-1">
                        <Container>
                            <h1>Clickatell</h1>
                            <FormGroup>
                                <Label for="exampleEmail">Clickatell Username</Label>
                                <Input type="text" name="text" id="exampleEmail" placeholder="with a placeholder" />
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleEmail">Clickatell Password</Label>
                                <Input type="text" name="text" id="exampleEmail" placeholder="with a placeholder" />
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleEmail">API ID</Label>
                                <Input type="text" name="text" id="exampleEmail" placeholder="with a placeholder" />
                            </FormGroup>
                            <Button color="info">Update</Button>
                        </Container>
                    </Card>
                </Col>
                <Col xl="4">
                    <Card className="shadow mb-1 mt-1">
                        <Container>
                            <h1>Twilio</h1>
                            <FormGroup>
                                <Label for="exampleEmail">Twilio Account SID</Label>
                                <Input type="text" name="text" id="exampleEmail" placeholder="with a placeholder" />
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleEmail">Authentication Token</Label>
                                <Input type="text" name="text" id="exampleEmail" placeholder="with a placeholder" />
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleEmail">Registered Phone Number</Label>
                                <Input type="text" name="text" id="exampleEmail" placeholder="with a placeholder" />
                            </FormGroup>
                            <Button color="info">Update</Button>
                        </Container>
                    </Card>
                </Col>
                <Col xl="4">
                    <Card className="shadow mb-1 mt-1">
                        <Container>
                            <h1>MSG91</h1>
                            <FormGroup>
                                <Label for="exampleEmail">Authentication Key SID</Label>
                                <Input type="text" name="text" id="exampleEmail" placeholder="with a placeholder" />
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleEmail">Sender ID</Label>
                                <Input type="text" name="text" id="exampleEmail" placeholder="with a placeholder" />
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleEmail">Route</Label>
                                <Input type="text" name="text" id="exampleEmail" placeholder="with a placeholder" />
                            </FormGroup>
                            <FormGroup>
                                <Label for="exampleEmail">Country Code</Label>
                                <Input type="text" name="text" id="exampleEmail" placeholder="with a placeholder" />
                            </FormGroup>
                            <Button color="info">Update</Button>
                        </Container>
                    </Card>
                </Col>
            </Row>
            </>
        );
    }
}

export default Sms;