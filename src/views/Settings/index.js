import React, { Component } from 'react';
import Header from "components/Headers/Header.js";
import { Card , Container, Row , Col } from 'reactstrap';
import { Route, Switch, Redirect , Link } from "react-router-dom";

import System_settng from "./System_settng";
import Sms from "./Sms";
import Email_set from "./Email_set";
import Terjemah from "./Terjemah";
import Lesson from "./Lesson";
import Database from "./Database";

class index extends Component {
    render()
    {
        return(
            <>
            <Header/>
            <Container className="mt--7" fluid>
            <Row>
                    <Col xl="12">
                        <Card>
                            <div className="d-flex justify-content-start mt-1">
                                <Link to="/user/settings">
                                <div className="text-center mt-1 ml-4 mr-5">
                                    <i className="fas fa-sliders-h" />
                                    <br/>
                                    <h5>SYSTEM SETTINGS</h5>
                                </div>
                                </Link>
                                <Link to="/user/settings/sms">
                                <div className="text-center mt-1 mr-5">
                                    <i className="fas fa-comment-alt" />
                                    <br/>
                                    <h5>SMS</h5>
                                </div>
                                </Link>
                                <Link to="/user/settings/email_settings">
                                <div className="text-center mt-1 mr-5">
                                    <i className="fas fa-envelope" />
                                    <br/>
                                    <h5>EMAIL SETTINGS</h5>
                                </div>
                                </Link>
                                <Link to="/user/settings/terjemah">
                                <div className="text-center mt-1 mr-5">
                                    <i className="fas fa-language" />
                                    <br/>
                                    <h5>TERJEMAH</h5>
                                </div>
                                </Link>
                                <Link to="/user/settings/database">
                                <div className="text-center mt-1 mr-5">
                                    <i className="fas fa-database" />
                                    <br/>
                                    <h5>DATABASE</h5>
                                </div>
                                </Link>
                            </div>
                            <div className="container">
                                <Switch>
                                    <Route path="/user/settings" exact={true} component={System_settng} key={1} />
                                    <Route path="/user/settings/sms" exact={true} component={Sms} key={2} />
                                    <Route path="/user/settings/email_settings" exact={true} component={Email_set} key={3} />
                                    <Route path="/user/settings/terjemah" exact={true} component={Terjemah} key={4} />
                                    <Route path="/user/settings/update/:lesson" exact={true} component={Lesson} key={5} />
                                    <Route path="/user/settings/database" exact={true} component={Database} key={6} />
                                    <Redirect from="*" to="/user/settings" />
                                </Switch>
                            </div>
                        </Card>
                    </Col>
                </Row>
            </Container>
            </>
        );
    }
}

export default index;