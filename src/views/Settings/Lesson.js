import React, { Component } from 'react';
import { Button, Input, Row, Col, Card , Container } from 'reactstrap';
import { Link } from "react-router-dom";

class Lesson extends Component {
    render(){
        return(
            <>
            <Link to="/user/settings/terjemah" className="btn btn-success">
                <i className="fas fa-arrow-circle-left" />
            </Link>
            <Button color="info">Tambahin</Button>
            <Row className="m-1 mt-3">
                <Col xl="3">
                    <Card>
                        <Container>
                            <p className="text-center">dashboard</p>
                            <div className="row justify-content-center m-1">
                                <Input type="text" value="dashboard" className="mb-1"/>
                                <Button color="success">Save</Button>
                            </div>
                        </Container>
                    </Card>
                </Col>

                <Col xl="3">
                    <Card>
                        <Container>
                            <p className="text-center">parent</p>
                            <div className="row justify-content-center m-1">
                                <Input type="text" value="parent" className="mb-1"/>
                                <Button color="success">Save</Button>
                            </div>
                        </Container>
                    </Card>
                </Col>

                <Col xl="3">
                    <Card>
                        <Container>
                            <p className="text-center">profile</p>
                            <div className="row justify-content-center m-1">
                                <Input type="text" value="profile" className="mb-1"/>
                                <Button color="success">Save</Button>
                            </div>
                        </Container>
                    </Card>
                </Col>

                <Col xl="3">
                    <Card>
                        <Container>
                            <p className="text-center">logout</p>
                            <div className="row justify-content-center m-1">
                                <Input type="text" value="logout" className="mb-1"/>
                                <Button color="success">Save</Button>
                            </div>
                        </Container>
                    </Card>
                </Col>

            </Row>
            </>
        );
    }
}

export default Lesson;