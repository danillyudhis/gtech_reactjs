import { FormGroup, Label, Input, Card , Container } from 'reactstrap';
import React, { Component } from 'react';
import { EditorState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

class Email_set extends Component {
    constructor(props) {
        super(props);
        this.state = {
          editorState: EditorState.createEmpty(),
        };
      }
    
      onEditorStateChange = (editorState) => {
        this.setState({
          editorState,
        });
      };
    
      render() {
        const { editorState } = this.state;
        return (
            <>
            <Card className="shadow">
                <Container>
                    <FormGroup>
                        <Label for="exampleEmail">Email Subject</Label>
                        <Input type="email" name="email"  placeholder="with a placeholder" />
                    </FormGroup>
                    <FormGroup>
                        <Editor
                        editorState={editorState}
                        toolbarClassName="toolbarClassName"
                        wrapperClassName="wrapperClassName"
                        editorClassName="editorClassName"
                        onEditorStateChange={this.onEditorStateChange}
                        />
                        <button className="btn btn-success mb-2">Update</button>
                    </FormGroup>
                </Container>
            </Card>
            </>
        )
      }
}

export default Email_set;