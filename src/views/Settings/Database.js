import React, { Component } from 'react';
import { Row, Col, Card , Container } from 'reactstrap';

class Database extends Component {
    render(){
        return(
            <>
                <h1>Database</h1>
                <Row className="mb-3">
                    <Col xl="6">
                        <Card>
                            <Container>
                                <div className="d-flex justify-content-center mt-3">
                                    <img src="https://new.disekolah.id/uploads/icons/backup.svg" alt="empty" style={{backgroundColor: '#fff', padding: 15, borderRadius: 0}} width="110px" />
                                </div>
                                <div className="d-flex justify-content-center">
                                    <p>Generate Backup</p>
                                </div>
                                <div className="d-flex justify-content-center mb-3">
                                    <button className="btn btn-success"><i className="fas fa-download" /></button>
                                </div>
                            </Container>
                        </Card>
                    </Col>
                    <Col xl="6">
                        <Card>
                            <Container>
                                <div className="d-flex justify-content-center mt-3">
                                    <img src="https://new.disekolah.id/uploads/icons/restore.svg" alt="empty" style={{backgroundColor: '#fff', padding: 15, borderRadius: 0}} width="110px" />
                                </div>
                                <div className="d-flex justify-content-center">
                                    <p>Import Backup</p>
                                </div>
                                <div className="d-flex justify-content-center mb-3">
                                    <button className="btn btn-info"><i className="fas fa-cloud-upload-alt" /></button>
                                </div>
                            </Container>
                        </Card>
                    </Col>
                </Row>
            </>
        );
    }
}

export default Database;