import React, { useState } from 'react';
import { Button, Table, Modal, ModalHeader, ModalBody, ModalFooter, Label, FormGroup, Input } from 'reactstrap';
import { Link } from "react-router-dom";

const Terjemah = (props) => {
    const {
      className
    } = props;

    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    return(
        <>
            <Table>
            <thead>
                <tr>
                <th><Button color="success" onClick={toggle}>Tambahin</Button></th>
                <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <th scope="row">English</th>
                <td><Link to="/user/settings/update/english" className="btn btn-info">Update</Link></td>
                </tr>
            </tbody>
            </Table>

            <Modal isOpen={modal} toggle={toggle} className={className}>
                <ModalHeader toggle={toggle}>Tambahin</ModalHeader>
                <ModalBody>
                    <FormGroup>
                    <Label for="exampleText">Nama</Label>
                    <Input type="text" name="text" id="exampleText" />
                    </FormGroup>
                    <FormGroup>
                    <Label for="exampleFile">Bendera</Label>
                    <Input type="file" name="file" id="exampleFile" />
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                <Button color="primary" onClick={toggle}>Do Something</Button>{' '}
                <Button color="secondary" onClick={toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </>
    );
}

export default Terjemah;