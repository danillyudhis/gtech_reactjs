import React, { useState } from 'react';
import { Card, CardText, Row, Col , Container, Modal, ModalHeader, ModalBody, ModalFooter , FormGroup, Input, Label} from 'reactstrap';
import { Link } from "react-router-dom";
import Header from "components/Headers/Header.js";

const Index = (props) => {
    const {
        className
      } = props;
    
      const [modal, setModal] = useState(false);
    
      const toggle = () => setModal(!modal);
      return (
          <>
          <Header/>

          <Container className="mt--7" fluid>
              <Row>
              <Col sm="3" className="mt-1">
                  <Card body onClick={toggle}>
                      <div className="text-center" style={{height: '11rem'}}>
                          <div style={{padding: '70px 0'}}>
                              <span><i className="fa fa-plus" aria-hidden="true"/></span>
                              <br/>
                              <h5 className="text-center">Buat tingkat baru</h5>
                          </div>
                      </div>
                  </Card>
              </Col>

              <Col sm="3" className="mt-1">
                  <Link to="/user/akademik/2">
                  <Card body> 
                      <div className="d-flex justify-content-center">
                          <img src="https://new.disekolah.id/uploads/3dcb5f9b376fcfe6f92fd1ebc205c7eeicon-logo-cyrcle.png"
                          style={{backgroundColor: '#fff', padding: 15, borderRadius: 0}} width="120px" alt="empty"/>
                      </div>
                   <br/>
                  <h5 className="text-center">Tingkat VII</h5>
                  <CardText>Sections: VII - A | VII - B |</CardText>
                  </Card>
                  </Link>
              </Col>

              <Col sm="3" className="mt-1">
                  <Link to="/user/akademik/1">
                  <Card body>
                      <div className="d-flex justify-content-center">
                          <img src="https://new.disekolah.id/uploads/3dcb5f9b376fcfe6f92fd1ebc205c7eeicon-logo-cyrcle.png"
                          style={{backgroundColor: '#fff', padding: 15, borderRadius: 0}} width="120px" alt="empty"/>
                      </div>
                   <br/>
                  <h5 className="text-center">Tingkat VIII</h5>
                  <CardText>Sections: VIII - A | VIII - B |</CardText>
                  </Card>
                  </Link>
              </Col>
              </Row>
          </Container>

            <>
            <Modal isOpen={modal} toggle={toggle} className={className}>
                    <ModalHeader toggle={toggle}>Buat Tingkat Baru</ModalHeader>
                    <ModalBody>
                            <FormGroup>
                                <Input type="text" name="nama" placeholder="Nama"/>
                            </FormGroup>
                            <FormGroup>
                                <Label>Nama Guru</Label>
                                <Input type="select" name="select" id="exampleSelect">
                                    <option>Udin Saefulloh S.Kom</option>
                                    <option>Teti  Kurnaeti, S.Pd </option>
                                    <option>Lily  Kamaliah, S,Pd.I</option>
                                    <option>Somad , S.Pd</option>
                                    <option>Suganda , S.Pd</option>
                                    <option>Maryanih , S.Pd</option>
                                    <option>Diah Arwulan, S.Pd</option>
                                </Input>
                            </FormGroup>
                            <button className="btn btn-success">Simpan</button>
                    </ModalBody>
                    <ModalFooter>
                    </ModalFooter>
                </Modal>
            </>

          </>
      );
    
}

export default Index;