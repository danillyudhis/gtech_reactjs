import React from 'react';
import { Container, Card } from 'reactstrap';
import { Route, Switch, Redirect , NavLink , Con} from "react-router-dom";
import Header from "components/Headers/Header.js";

const Detail = (props) => {

  return (
      <>
      <Header/>
      <Container className="mt--7" fluid>
          <Card>
              <div className="ml-5 mt-3">
                <img alt src="https://new.disekolah.id/uploads/subject_icon/f9ed9d7cd0e4c4ced46d9990eb0dfa86subject-icon-5.jpg" style={{width: 60}} />
                <p><h5>Bahasa Sunda - Dashboard</h5></p>
                <p>Tingkat VII "VII - A"</p>
              </div>
              <div className="d-flex justify-content-start mt-1">
                    {/* <NavLink to="/user/pendaftaran" exact={true} activeClassName="text-warning"> */}
                    <div className="text-center mt-1 ml-4 mr-5">
                        <i className="fas fa-tachometer-alt" />
                        <br/>
                        <h3>Dashboard</h3>
                    </div>
                    {/* </NavLink> */}
                    {/* <NavLink to="/user/pendaftaran/wali_siswa" exact={true} activeClassName="text-warning"> */}
                    <div className="text-center mt-1 mr-5">
                        <i className="fas fa-stream" />
                        <br/>
                        <h3>Online Exams</h3>
                    </div>
                    {/* </NavLink> */}
                    {/* <NavLink to="/user/pendaftaran/pelengkap" exact={true} activeClassName="text-warning"> */}
                    <div className="text-center mt-1 mr-5">
                        <i className="fas fa-book" />
                        <h3>Homework</h3>
                    </div>
                    {/* </NavLink> */}
                    <div className="text-center mt-1 mr-5">
                        <h3>Forum</h3>
                    </div>
                    <div className="text-center mt-1 mr-5">
                        <h3>Study Material</h3>
                    </div>
                    <div className="text-center mt-1 mr-5">
                        <h3>Marks</h3>
                    </div>
                    <div className="text-center mt-1 mr-5">
                        <h3>Live</h3>
                    </div>
              </div>
          </Card> 
      </Container>
    </>
  );
  
}

export default Detail;
