import React, { Component } from 'react';
import { Route, Switch, Redirect } from "react-router-dom";

// list Component 
import Index from "./Index.js";
import List from "./List.js";
import Detail from "./Detail.js";

class Akademik extends Component {
    render(){
        return (
            <>
                <Switch>
                    <Route path="/user/akademik" exact={true} component={Index} key={1} />
                    <Route path="/user/akademik/:id" exact={true} component={List} key={2} />
                    <Route path="/user/akademik/:id_subject/:id_pelajaran" exact={true} component={Detail} key={3} />
                    <Redirect from="*" to="/admin/akademik" />
                </Switch>
            </>
        );
    }
}

export default Akademik;