import React, { useState } from 'react';
import { Card, CardText, Row, Col , Container, Modal, ModalHeader, ModalBody, ModalFooter , FormGroup, Input, Label} from 'reactstrap';
import { Link } from "react-router-dom";
import Header from "components/Headers/Header.js";

const List = (props) => {
    const {
        className
      } = props;
    
    const [modal, setModal] = useState(false);
    
    const toggle = () => setModal(!modal);
        return(
        <>
        <Header/>
            <Container className="mt--7" fluid>
            <Row>
            <Col sm="3" className="mt-1">
                <Card body onClick={toggle}>
                    <div className="text-center" style={{height: '12rem'}}>
                        <div style={{padding: '70px 0'}}>
                            <i className="fa fa-plus" aria-hidden="true"/>
                            <br/>
                            <h5 className="text-center">Bikin Mata Pelajaran Baru</h5>
                        </div>
                    </div>
                </Card>
            </Col>
            <Col sm="3" className="mt-1">
                <Link to={`/user/akademik/${props.match.params.id}/${'12'}`}>
                <Card body> 
                    <div className="d-flex justify-content-center">
                    <img src="https://new.disekolah.id/uploads/subject_icon/f9ed9d7cd0e4c4ced46d9990eb0dfa86subject-icon-5.jpg" alt="empty" style={{backgroundColor: '#0084FF', padding: 30, borderRadius: 100}} width="120px" />
                    </div>
                <br/>
                <CardText className="text-center mt-1">Bahasa Sunda - VII - A</CardText>
                <h5 className="text-center"> Teti Kurnaeti, S.Pd </h5>
                </Card>
                </Link>
            </Col>
            <Col sm="3" className="mt-1">
                <Link to={`/user/akademik/${props.match.params.id}/${'12'}`}>
                <Card body> 
                    <div className="d-flex justify-content-center">
                    <img src="https://new.disekolah.id/uploads/subject_icon/f9ed9d7cd0e4c4ced46d9990eb0dfa86subject-icon-5.jpg" alt="empty" style={{backgroundColor: '#0084FF', padding: 30, borderRadius: 100}} width="120px" />
                    </div>
                <br/>
                <CardText className="text-center mt-1">Bahasa Inggris - VII - A</CardText>
                <h5 className="text-center">Hesni Kusna</h5>
                </Card>
                </Link>
            </Col>
            <Col sm="3" className="mt-1">
                <Link to={`/user/akademik/${props.match.params.id}/${'12'}`}>
                <Card body> 
                    <div className="d-flex justify-content-center">
                    <img src="https://new.disekolah.id/uploads/subject_icon/f9ed9d7cd0e4c4ced46d9990eb0dfa86subject-icon-5.jpg" alt="empty" style={{backgroundColor: '#0084FF', padding: 30, borderRadius: 100}} width="120px" />
                    </div>
                <br/>
                <CardText className="text-center mt-1">Bahasa Jepang - VII - A</CardText>
                <h5 className="text-center">Hengki Kusna</h5>
                </Card>
                </Link>
            </Col>
            </Row>
        </Container>
            <>
            <Modal isOpen={modal} toggle={toggle} className={className}>
                    <ModalHeader toggle={toggle}>Mata Pelajaran baru</ModalHeader>
                    <ModalBody>
                            <FormGroup>
                                <Label>Nama Pelajaran</Label>
                                <Input type="text" name="nama" placeholder="Nama"/>
                            </FormGroup>
                            <FormGroup>
                                <Label>Tingkat</Label>
                                <Input type="select" name="tingkat">
                                    <option>Tingkat VII</option>
                                    <option>Tingkat VIII</option>
                                    <option>Tingkat IX</option>
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label>Kelas</Label>
                                <Input type="select" name="kelas">
                                    <option>Pilih</option>
                                    <option>VII - A</option>
                                    <option>VII - B</option>
                                </Input>
                            </FormGroup>
                            <FormGroup>
                                <Label>Guru</Label>
                                <Input type="select" name="kelas">
                                    <option>Pilih</option>
                                    <option>Udin Saefulloh S.Kom</option>
                                    <option>Teti  Kurnaeti, S.Pd</option>
                                    <option>Somad , S.Pd</option>
                                </Input>
                            </FormGroup>
                            <button className="btn btn-success">Simpan</button>
                    </ModalBody>
                    <ModalFooter>
                    </ModalFooter>
                </Modal>
            </>
        </>
    );
}

export default List;