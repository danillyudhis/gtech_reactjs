import React, { useState } from 'react';
import { Table , Button , Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Label, Input} from 'reactstrap';
import swal from 'sweetalert';

// delete pop up confirmation
const del_pop = () => {
    swal({
      title: "Are you sure delete this?",
      text: "Once deleted, you will not be able to recover this!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        swal("Poof! Your data was successfully delete!", {
          icon: "success",
        });
      } else {
        swal("Your Data was savfe!");
      }
    });   
}

const Tingkatan = (props) => {
  const {
    className
  } = props;

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);
    
  return(
      <>
      <div className="row justify-content-between mb-3 mt-3">
          <div className="ml-4">
              <h4>Tingkatan Nilai</h4>
          </div>
          <div className="mr-4">
              <Button color="success" onClick={toggle}><i className="fas fa-plus" /> Baru </Button>
          </div>
      </div>
      <Table>
      <thead>
          <tr>
          <th>NAMA</th>
          <th>POINT</th>
          <th>DARI NILAI</th>
          <th>SAMPAI NILAI</th>
          <th>OPTIONS</th>
          </tr>
      </thead>
      <tbody>
          <tr>
          <th>A+</th>
          <td>4</td>
          <td>90</td>
          <td>100</td>
          <td>
              <i className="far fa-edit fa-lg mr-3" />
              <i className="fas fa-trash fa-lg" onClick={(e) => del_pop()}/>
          </td>
          </tr>
      </tbody>
      </Table>

      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>Modal title</ModalHeader>
        <ModalBody>
            <FormGroup>
            <Label>Nama</Label>
            <Input type="text" name="email" placeholder="with a placeholder" />
            </FormGroup>
            <FormGroup>
            <Label>Point</Label>
            <Input type="number" name="email"  placeholder="with a placeholder" />
            </FormGroup>
            <FormGroup>
            <Label>Dari Nilai</Label>
            <Input type="number" name="email"  placeholder="with a placeholder" />
            </FormGroup>
            <FormGroup>
            <Label>Sampai Nilai</Label>
            <Input type="number" name="email"  placeholder="with a placeholder" />
            </FormGroup>
        </ModalBody>
        <ModalFooter>
          <Button color="success">Save</Button>{' '}
          <Button color="secondary" onClick={toggle}>Cancel</Button>
        </ModalFooter>
      </Modal>
      </>
  );
}

export default Tingkatan;