import React, { Component } from 'react';
import { Card, Container } from 'reactstrap';
import { Route, Switch, Redirect , Link } from "react-router-dom";
import Header from "components/Headers/Header.js";

import Pengaturan from "./Pengaturan";
import Section from "./Section";
import Tingkatan from "./Tingkatan";
import Semester from "./Semester";
import Student_promotion from "./Student_promotion";

class index extends Component {
    render() {
        return(
            <>
            <Header/>
            <Container className="mt--7" fluid>
                <Card>
                <div className="d-flex justify-content-start mt-1">
                    <Link to="/user/pengaturan_akademik">
                    <div className="text-center mt-1 ml-4 mr-5">
                        <i className="fas fa-trophy" />
                        <br/>
                        <h5>PENGATURAN AKADEMIK</h5>
                    </div>
                    </Link>
                    <Link to="/user/pengaturan_akademik/section">
                    <div className="text-center mt-1 mr-5">
                        <i className="fas fa-user-graduate" />
                        <br/>
                        <h5>SECTION</h5>
                    </div>
                    </Link>
                    <Link to="/user/pengaturan_akademik/tingkatan_nilai">
                    <div className="text-center mt-1 mr-5">
                        <i className="far fa-calendar-alt" />
                        <br/>
                        <h5>TINGKATAN NILAI</h5>
                    </div>
                    </Link>
                    <Link to="/user/pengaturan_akademik/semester">
                    <div className="text-center mt-1 mr-5">
                        <i className="fas fa-clipboard-check" />
                        <br/>
                        <h5>SEMESTER</h5>
                    </div>
                    </Link>
                    <Link to="/user/pengaturan_akademik/student_promotion">
                    <div className="text-center mt-1 mr-5">
                        <i className="fas fa-scroll" />
                        <br/>
                        <h5>STUDENT PROMOTION</h5>
                    </div>
                    </Link>
                </div>
                <Container>
                <Switch>
                    <Route path="/user/pengaturan_akademik" exact={true} component={Pengaturan} key={1} />
                    <Route path="/user/pengaturan_akademik/section" exact={true} component={Section} key={2} />
                    <Route path="/user/pengaturan_akademik/tingkatan_nilai" exact={true} component={Tingkatan} key={3} />
                    <Route path="/user/pengaturan_akademik/semester" exact={true} component={Semester} key={4} />
                    <Route path="/user/pengaturan_akademik/student_promotion" exact={true} component={Student_promotion} key={5} />
                    <Redirect from="*" to="/user/pengaturan_akademik" />
                </Switch>
                </Container>
                </Card>
            </Container>
            </>
        );
    }
}

export default index;