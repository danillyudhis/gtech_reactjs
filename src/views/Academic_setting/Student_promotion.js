import React, { Component } from 'react';
import { Row, Col, Input, Label, Button , Table } from 'reactstrap';

class Student_promotion extends Component {
    render() {
        return(
        <>
        <Row className="mt-4">
            <Col xl="2">
                <Label>Running Year</Label>
                <Input type="select" name="select">
                <option>2020</option>
                </Input>
            </Col>
            <Col xl="2">
                <Label>Year To Promote</Label>
                <Input type="select" name="select">
                <option>2021</option>
                </Input>
            </Col>
            <Col xl="2">
                <Label>Kelas Sekarang</Label>
                <Input type="select" name="select">
                <option>Tingkat VII</option>
                <option>Tingkat VIII</option>
                <option>Tingkat IX</option>
                </Input>
            </Col>
            <Col xl="3">
                <Label>Kelas To Promote</Label>
                <Input type="select" name="select">
                <option>Tingkat VII</option>
                <option>Tingkat VIII</option>
                <option>Tingkat IX</option>
                </Input>
            </Col>
            <Col xl="2">
                <Label style={{color: 'white'}}>dsaaaaa</Label>
                <Button color="success">Promote</Button>
            </Col>
        </Row>
        <div className="mt-4">
            <Table>
            <thead>
                <tr>
                <th>Nama</th>
                <th>Kelas</th>
                <th>Roll ID</th>
                <th>Options</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <th>Lakeswara Karisma</th>  
                <td>IX - A</td>
                <td>12345690</td>
                <td>
                    <Input type="select" name="select">
                    <option>Promote To - Tingkat VII</option>
                    <option>Promote To - Tingkat VIII</option>
                    </Input>
                </td>
                </tr>
            </tbody>
            </Table>
        </div>
        </>
        );
    }
}

export default Student_promotion;