import React, { useState } from 'react';
import { Input, Row, Col, Card, Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Label } from 'reactstrap';

const Section = (props) => {
    const {
        className
    } = props;
    
    const [modal, setModal] = useState(false);
    
    const toggle = () => setModal(!modal);
    
    return(
        <>
        <h4>Manage Section</h4>
        <div className="row justify-content-between mt-2">
        <div className="col-4">
            <Label>Pilih Tingkat</Label>
            <Input type="select" name="select" id="exampleSelect">
            <option>Pilih</option>
            <option>Tingkat VII</option>
            <option>Tingkat VIII</option>
            </Input>
        </div>
        <div className="col-4">
            <button type="button" className="btn btn-success" style={{borderRadius: '50%;', marginTop: '30px'}} onClick={toggle}><i className="far fa-edit" /></button>
        </div>
        </div>
        <Row className="mt-3 mb-2">
            <Col xl="4">
                <Card>
                    <div className="container">
                        <h5>Guru : Udin Saefulloh S.Kom</h5>
                        <h5>Siswa: 4</h5>
                        <h5>Tingkat: <span class="badge badge-pill badge-info">Tingkat VII</span></h5>
                    </div>
                </Card>
            </Col>
            <Col xl="4">
                <Card>
                    <div className="container">
                        <h5>Guru : Amber S.Kom</h5>
                        <h5>Siswa: 4</h5>
                        <h5>Tingkat: <span class="badge badge-pill badge-info">Tingkat VII</span></h5>
                    </div>
                </Card>
            </Col>
            <Modal isOpen={modal} toggle={toggle} className={className}>
                <ModalHeader toggle={toggle}>New Section</ModalHeader>
                <ModalBody>
                    <FormGroup>
                        <Input type="text" name="password" placeholder="Name" />
                    </FormGroup>
                    <FormGroup>
                        <Label>Guru</Label>
                        <Input type="select" name="select" id="exampleSelect">
                        <option>PIlih</option>
                        <option>Udin Saefulloh</option>
                        <option>Dadang Muhammad</option>
                        </Input>
                    </FormGroup>
                    <FormGroup>
                        <Label>Tingkat</Label>
                        <Input type="select" name="select" id="exampleSelect">
                        <option>PIlih</option>
                        <option>Tingkat VII</option>
                        <option>Tingkat VIII</option>
                        </Input>
                    </FormGroup>
                </ModalBody>
                <ModalFooter>
                <Button color="success">Save</Button>{' '}
                <Button color="secondary" onClick={toggle}>Close</Button>
                </ModalFooter>
            </Modal>
        </Row>
        </>
    );
}

export default Section;