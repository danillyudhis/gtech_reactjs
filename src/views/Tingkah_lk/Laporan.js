import React, { Component } from 'react';
import { Table } from 'reactstrap';

class Laporan extends Component {
    constructor(props) {
        super(props);
        this.state = { view: 'siswa' };
    }
    
    render() {
        console.log(this.state)
        return(
            <>
            <div className="row mb-3 mt-3 ml-2">
                <button className="col-sm-1 btn btn-info" onClick={() => this.setState({view:'siswa'})}>Siswa</button>
                <button className="col-sm-1 btn btn-info" onClick={() => this.setState({view:'guru'})}>Guru</button>
            </div>
                {this.state.view === 'siswa' ? <Siswa/> : <Guru/>}
            </>
        );        
    }
    
}

function Siswa(props){
    return(
        <Table>
        <thead>
            <tr>
            <th>Prioritas</th>
            <th>Tanggal</th>
            <th>Dibuat Oleh</th>
            <th>Siswa</th>
            <th>Tingkat</th>
            <th>Kelas</th>
            <th>Judul</th>
            <th>Options</th>
            </tr>
        </thead>
        <tbody>
            {/* <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
            </tr> */}
        </tbody>
        </Table>
    );
}

function Guru(props) {
    // Prioritas 	Tanggal 	Dibuat oleh 	Guru 	Judul 	Options
    return(
        <Table>
        <thead>
            <tr>
            <th>Prioritas</th>
            <th>Tanggal</th>
            <th>Dibuat Oleh</th>
            <th>Oleh</th>
            <th>Guru</th>
            <th>Judul</th>
            <th>Options</th>
            </tr>
        </thead>
        <tbody>
            {/* <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
            </tr> */}
        </tbody>
        </Table>
    );
}



export default Laporan;