import React, { Component } from 'react';
import { Table } from 'reactstrap';

class Izin extends Component {
    constructor(props) {
        super(props);
        this.state = { view: 'siswa' };
    }

    render(){
        return(
            <>
            <div className="row mb-3 mt-3 ml-2">
                <button className="col-sm-1 btn btn-info" onClick={() => this.setState({view:'siswa'})}>Siswa</button>
                <button className="col-sm-1 btn btn-info" onClick={() => this.setState({view:'guru'})}>Guru</button>
            </div>
                {this.state.view === 'siswa' ? <Siswa/> : <Guru/>}
            </>
        );
    }
}

function Siswa(props){
    return(
        <Table>
        <thead>
            <tr>
            <th>Judul</th>
            <th>Keterangan</th>
            <th>Siswa</th>
            <th>From</th>
            <th>Until</th>
            <th>Status</th>
            <th>Options</th>
            </tr>
        </thead>
        <tbody>
            {/* <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
            </tr> */}
        </tbody>
        </Table>
    );
}

function Guru(props) {
    // Judul 	Keterangan 	Guru 	From 	Until 	Options
    return(
        <Table>
        <thead>
            <tr>
            <th>Judul</th>
            <th>Keterangan</th>
            <th>Guru</th>
            <th>From</th>
            <th>Until</th>
            <th>Options</th>
            </tr>
        </thead>
        <tbody>
            {/* <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
            </tr> */}
        </tbody>
        </Table>
    );
}

export default Izin;