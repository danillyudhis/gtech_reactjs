import React, { Component } from 'react';
import { Container, Card } from 'reactstrap';
import { Route, Switch, Redirect , Link } from "react-router-dom";
import Header from "components/Headers/Header.js";

import Laporan from "./Laporan";
import Izin from "./Izin";
class index extends Component {
    render() {
        return(
            <>
            <Header/>
            <Container className="mt--7" fluid>
                <Card>
                <div className="d-flex justify-content-start mt-1">
                    <Link to="/user/tingkah_laku">
                    <div className="text-center mt-1 ml-4 mr-5">
                        <i className="fas fa-gavel" />
                        <br/>
                        <h5>Laporan</h5>
                    </div>
                    </Link>
                    <Link to="/user/tingkah_laku/izin">
                    <div className="text-center mt-1 ml-4 mr-5">
                        <i className="fas fa-tree" />
                        <br/>
                        <h5>Izin</h5>
                    </div>
                    </Link>
                </div>
                <Container>
                <Switch>
                    <Route path="/user/tingkah_laku" exact={true} component={Laporan} key={1} />
                    <Route path="/user/tingkah_laku/izin" exact={true} component={Izin} key={2} />
                    <Redirect from="*" to="/user/tingkah_laku" />
                </Switch>
                </Container>
                </Card>
            </Container>
            </>
        );
    }
}

export default index;