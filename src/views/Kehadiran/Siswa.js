import React, { Component } from 'react';
import { Label, Input, Row, Col, Container, Table} from 'reactstrap';

class Siswa extends Component {
    render() {
        return(
            <>
            <Container>
                <Row>
                    <Col xl="3">    
                        <Label>Tingkat</Label>
                        <Input type="select" name="tingkat">
                        <option>Pilih</option>
                        <option>Tingkat VII</option>
                        <option>Tingkat VIII</option>
                        <option>Tingkat IX</option>
                        <option>Private</option>
                        </Input>
                    </Col>
                    <Col xl="3">    
                        <Label>Kelas</Label>
                        <Input type="select" name="select">
                        <option>Pilih</option>
                        </Input>
                    </Col>
                    <Col xl="3">    
                        <Label>Tanggal</Label>
                        <Input type="date" name="date" placeholder="date placeholder" />
                    </Col>
                    <Col xl="3">
                        <button className="btn btn-success" style={{marginTop: '30px'}}>Cari</button>
                    </Col>
                </Row>
                
                <Table className="mt-2">
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Siswa</th>
                    <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                    </tr>
                    <tr>
                    <th scope="row">2</th>
                    <td>Jacob</td>
                    <td>Thornton</td>
                    </tr>
                    <tr>
                    <th scope="row">3</th>
                    <td>Larry</td>
                    <td>the Bird</td>
                    </tr>
                </tbody>
                </Table>
            </Container>
            </>
        );
    }
}

export default Siswa;