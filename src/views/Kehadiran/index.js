import React, { Component } from 'react';
import { Container, Row, Col, Card, Navbar, NavItem, Nav } from 'reactstrap';
import { Route, Switch, Redirect , Link } from "react-router-dom";
import Header from "components/Headers/Header.js";

import Siswa from "./Siswa.js";
import Guru from "./Guru.js";

class index extends Component {
    render() {
        return(
            <>
            <Header/>
            <Container className="mt--7" fluid>
                <Row>
                    <Col xl="12">

                        <Card>
                            <Navbar expand="md" className="d-flex justify-content-start" style={{background: 'white'}}>
                                <Nav className="d-flex justify-content-between" navbar>
                                    <NavItem className="navs-item text-center">
                                        <Link to="/user/kehadiran">
                                        <i className="fa fa-users" aria-hidden="true"></i>
                                        <br/>
                                        <span>KEHADIRAN SISWA</span>
                                        </Link>
                                    </NavItem>
                                
                                    <NavItem className="navs-item text-center ml-4 mr-4">
                                        <Link to="/user/kehadiran/guru">
                                        <i className="fas fa-chalkboard-teacher"></i>
                                        <br/>
                                        <span>KEHADIRAN GURU</span>
                                        </Link>
                                    </NavItem>
                                
                                    <NavItem className="navs-item text-center">
                                        <i className="far fa-chart-bar"></i>
                                        <br/>
                                        <span>LAPORAN KEHADIRAN GURU</span>
                                    </NavItem>
                                </Nav>
                            </Navbar>
                        </Card>

                    </Col>

                    <Col xl="12" className="mt-1">
                        <Card>
                            <Switch>
                                <Route path="/user/kehadiran" exact={true} component={Siswa} key={1} />
                                <Route path="/user/kehadiran/guru" exact={true} component={Guru} key={1} />
                                <Redirect from="*" to="/user/kehadiran" />
                            </Switch>
                        </Card>
                    </Col>

                </Row>
            </Container>
            </>
        );
    }
}

export default index;