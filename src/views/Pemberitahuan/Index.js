import React, { useState } from 'react';
import { EditorState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import { Container , Row, Col, Card , Modal, ModalHeader, ModalBody, ModalFooter, Button, Input , FormGroup, Label } from 'reactstrap';
import Header from "components/Headers/Header.js";
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

function Index(props) {
        const {
          className
        } = props;
    
        const [modal, setModal] = useState(false);
        const [view, setView] = useState(false);
    
        const toggle = () => setModal(!modal);

        const sh_modal = (type) => {
            setModal(!modal);
            setView(type)
        }

        return(
            <>
            <Header/>
            <Container className="mt--7" fluid>
                <Row>
                    <Col xl="6">
                        <Card>
                            <div className="container mb-2 mt-2">
                                <div className="d-flex justify-content-center">
                                    <img src="https://new.disekolah.id/uploads/icons/sms.svg" alt="empty" style={{backgroundColor: '#fff', padding: 15, borderRadius: 0}} width="110px" />
                                </div>
                                <div className="text-center">
                                    <p>Sends SMS</p>
                                    <p>Available for all users</p>
                                </div>
                                <div className="d-flex justify-content-center">
                                    <button className="btn btn-success" onClick={() => sh_modal('sms')}>Send SMS</button>
                                </div>
                            </div>
                        </Card>
                    </Col>
                    <Col xl="6">
                        <Card>
                            <div className="container mb-2 mt-2">
                                <div className="d-flex justify-content-center">
                                    <img src="https://new.disekolah.id/uploads/icons/emails.svg" alt="empty" style={{backgroundColor: '#fff', padding: 15, borderRadius: 0}} width="110px" />
                                </div>
                                <div className="text-center">
                                    <p>Send Emails</p>
                                    <p>Available for all users</p>
                                </div>
                                <div className="d-flex justify-content-center">
                                    <button className="btn btn-success" onClick={() => sh_modal('email')}>Add Email</button>
                                </div>
                            </div>
                        </Card>
                    </Col>
                </Row>
            </Container>
    
            <Modal isOpen={modal} toggle={toggle} className={className}>
                <ModalHeader toggle={toggle}>Modal title</ModalHeader>
                <ModalBody>
                    {view === 'sms' ? <Sms_form/>: <Sn_email/>}
                </ModalBody>
                <ModalFooter>
                <Button color="primary" onClick={toggle}>Do Something</Button>{' '}
                <Button color="secondary" onClick={toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
            </>
        );
}

function Sms_form() {
    return(
        <>
        <FormGroup>
            <Label>Penerima</Label>
            <Input type="select" name="select" id="exampleSelect">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
            </Input>
        </FormGroup>
        <FormGroup>
            <Label>Message</Label>
            <Input type="textarea" name="text" id="exampleText" />
        </FormGroup>
        </>
    );
}

function Sn_email(props) {
    const [editorState, setEditor] = useState(EditorState.createEmpty());

    function onEditorStateChange(editorState) {
        setEditor(editorState)
    };
    return(
        <>
        <FormGroup>
            <Label>Penerima</Label>
            <Input type="select" name="select" id="exampleSelect">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
            </Input>
        </FormGroup>
        <FormGroup>
            <Label>Email Subject</Label>
            <Input type="text" name="text" id="exampleText" />
        </FormGroup>
        <FormGroup className="shadow">
                <Editor
                editorState={editorState}
                toolbarClassName="toolbarClassName"
                wrapperClassName="wrapperClassName"
                editorClassName="editorClassName"
                onEditorStateChange={onEditorStateChange}
                />
        </FormGroup>
        </>
    );
}

export default Index;