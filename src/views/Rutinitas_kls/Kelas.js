import React, { useState} from 'react';
import {
    Row,
    Col,
    Input,
    Label,
    Modal, ModalHeader, ModalBody, ModalFooter, Button, FormGroup
  } from "reactstrap";

const Kelas = (props) => {
    
    const {
      buttonLabel,
      className
    } = props;

    const [modal, setModal] = useState(false);

    const toggle = () => setModal(!modal);

    return(
        <>
        <Row>
            <Col xl="8">
                <Label>Filter Kelasnya</Label>
                <Input type="select" name="select">
                <option>Pilih</option>
                <option>Tingkat VII</option>
                <option>Tingkat VIII</option>
                <option>Tingkat IX</option>
                <option>Private</option>
                </Input>
            </Col>
            <Col xl="4">
                <br className="mt-1"/>
                <button className="btn btn-success" onClick={toggle}> + Add Class Routine </button>
            </Col>
        </Row>
        <h5 className="mt-5">KELAS VII - A KELAS VII - B</h5>
        <hr/>
        <Modal isOpen={modal} toggle={toggle} className={className}>
            <ModalHeader toggle={toggle}>Tambah Rutinitas Kelas</ModalHeader>
            <ModalBody>
                <Row>
                    <Col xl='6'>
                    <FormGroup>
                        <Label>Tingkat</Label>
                        <Input type="select" name="select">
                        <option>Tingkat VII</option>
                        <option>TIngkat VIII</option>
                        <option>Tingkat X</option>
                        </Input>
                    </FormGroup>
                    </Col>
                    <Col xl='6'>
                    <FormGroup>
                        <Label>Kelas</Label>
                        <Input type="select" name="select">
                        <option>VII Dude</option>
                        <option>VIII Just</option>
                        <option>X Aden</option>
                        </Input>
                    </FormGroup>
                    </Col>
                </Row>
                <Row>
                    <Col xl='6'>
                    <FormGroup>
                        <Label>Subject</Label>
                        <Input type="select" name="select">
                        <option>Pilih</option>
                        </Input>
                    </FormGroup>
                    </Col>
                    <Col xl='6'>
                    <FormGroup>
                        <Label>Hari</Label>
                        <Input type="select" name="select">
                        <option>Senin</option>
                        <option>Selasa</option>
                        <option>Rabu</option>
                        <option>Kamis</option>
                        <option>Jumat</option>
                        <option>Sabtu</option>
                        <option>Minggu</option>
                        </Input>
                    </FormGroup>
                    </Col>
                </Row>
                <h4>Start</h4>
                <Row>
                    <Col xl='4'>
                    <FormGroup>
                        <Label>Jam</Label>
                        <Input type="select" name="select">
                        <option>Pilih</option>
                        </Input>
                    </FormGroup>
                    </Col>
                    <Col xl='4'>
                    <FormGroup>
                        <Label>Menit</Label>
                        <Input type="select" name="select">
                        <option>Pilih</option>
                        </Input>
                    </FormGroup>
                    </Col>
                    <Col xl='4'>
                    <FormGroup>
                        <Label>Menit</Label>
                        <Input type="select" name="select">
                        <option>Senin</option>
                        <option>Selasa</option>
                        <option>Rabu</option>
                        <option>Kamis</option>
                        <option>Jumat</option>
                        <option>Sabtu</option>
                        <option>Minggu</option>
                        </Input>
                    </FormGroup>
                    </Col>
                </Row>
                <h4>End</h4>
                <Row>
                    <Col xl='4'>
                    <FormGroup>
                        <Label>Jam</Label>
                        <Input type="select" name="select">
                        <option>Pilih</option>
                        </Input>
                    </FormGroup>
                    </Col>
                    <Col xl='4'>
                    <FormGroup>
                        <Label>Menit</Label>
                        <Input type="select" name="select">
                        <option>Pilih</option>
                        </Input>
                    </FormGroup>
                    </Col>
                    <Col xl='4'>
                    <FormGroup>
                        <Label>Menit</Label>
                        <Input type="select" name="select">
                        <option>Senin</option>
                        <option>Selasa</option>
                        <option>Rabu</option>
                        <option>Kamis</option>
                        <option>Jumat</option>
                        <option>Sabtu</option>
                        <option>Minggu</option>
                        </Input>
                    </FormGroup>
                    </Col>
                </Row>
            </ModalBody>
            <ModalFooter>
            <Button color="success">Tambah</Button>{' '}
            <Button color="secondary" onClick={toggle}>Cancel</Button>
            </ModalFooter>
        </Modal>
        </>
    );
    
}

export default Kelas;