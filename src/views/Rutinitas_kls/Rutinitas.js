import React  from 'react'
import {
    Card,
    Container,
    Row,
    Col,
    Input,
    Label
  } from "reactstrap";
import Header from "components/Headers/Header.js";
import { Route, Switch, Redirect , NavLink } from "react-router-dom";

import Kelas from "./Kelas";
import Guru from "./Guru";
import Exam from "./Exam";

const Rutinitas = () => {

    return(
        <>
            <Header/>
            <Container className="mt--7" fluid>
                    <Card>
                        <nav className="navbar navbar-expand-lg">
                    
                        <Row>
                            <Col xl="3" className="text-center">
                                <NavLink to="/user/rutinitas_kelas" exact={true} activeClassName="text-success">
                                    <i className="far fa-calendar-alt"></i>
                                    <span className="nav-link">Rutinitas Kelas</span>
                                </NavLink>
                            </Col>
                            <Col xl="3" className="text-center">
                                <NavLink to="/user/rutinitas_kelas/guru" exact={true} activeClassName="text-success">
                                    <i className="fas fa-glasses"></i>
                                    <span className="nav-link">Rutinitas Guru</span>
                                </NavLink>
                            </Col>
                            <Col xl="3" className="text-center">
                                <NavLink to="/user/rutinitas_kelas/exam" exact={true} activeClassName="text-success">
                                    <i className="fas fa-book"></i>
                                    <span className="nav-link">Exam Routine</span>
                                </NavLink>
                            </Col>
                        </Row>

                        </nav>

                        <Row>
                            <Container fluid>    
                            <Switch>
                                <Route path="/user/rutinitas_kelas" exact={true} component={Kelas} key={1} />
                                <Route path="/user/rutinitas_kelas/guru" exact={true} component={Guru} key={2} />
                                <Route path="/user/rutinitas_kelas/exam" exact={true} component={Exam} key={3} />
                                <Redirect from="*" to="/user/rutinitas_kelas" />
                            </Switch>
                            </Container>
                        </Row>
                    </Card>
            </Container>
        </>
    );
    
}

export default Rutinitas;