import React, { Component } from 'react';
import {
    Card,
    Container,
    Row,
    Col,
    Input,
    Label
  } from "reactstrap";

class Guru extends Component {
    render(){
        return(
            <>
            <Row>
                <Col xl="8">
                    <Label>Guru</Label>
                    <Input type="select" name="select">
                    <option>Pilih</option>
                    <option>Tingkat VII</option>
                    <option>Tingkat VIII</option>
                    <option>Tingkat IX</option>
                    <option>Private</option>
                    </Input>
                </Col>
            </Row>
            <hr/>
            </>
        );
    }
}

export default Guru;