import React, { Component } from 'react';
import {
    Card,
    Container,
    Row,
    Col,
    ListGroup, ListGroupItem
  } from "reactstrap";

import Header from "components/Headers/Header.js";
import Cookies from 'universal-cookie';
import Json_extractor from "../Json_extractor"; // extract session data

const jwt = require('jsonwebtoken');
const cookies = new Cookies();
const jwt_key = require("../Jwt_key");

const encrypt_sessn = jwt.verify(Json_extractor(cookies.get('session')), jwt_key.key); // calling session data and then decrypt session with jwt

class Dashboard extends Component {
    constructor(props) {
        super(props);
    }

    
    render() {
        return (
            <> 
                <Header/>
                <Container className="mt--7" fluid>
                    <Row>
                        <main className="col col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">
                                <Card className="shadow mt-2">
                                    <div className="d-flex justify-content-center">
                                        <img src="https://new.disekolah.id/uploads/3dcb5f9b376fcfe6f92fd1ebc205c7eeicon-logo-cyrcle.png" width="100" height="100" alt="Aplikasi disekolah"/>
                                    </div>
                                </Card>
                        </main>
                        <Col className="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">
                                <Card className="shadow mt-2">
                                    <div className="d-flex justify-content-center">
                                        <img src="https://new.disekolah.id/uploads/3dcb5f9b376fcfe6f92fd1ebc205c7eeicon-logo-cyrcle.png" width="100" height="100" alt="Aplikasi disekolah"/>
                                    </div>
                                </Card>
                        </Col>
                        <Col className="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">
                                <Card className="shadow mt-2">
                                    <div className="d-flex justify-content-center">
                                        <img src="https://new.disekolah.id/uploads/3dcb5f9b376fcfe6f92fd1ebc205c7eeicon-logo-cyrcle.png" width="100" height="100" alt="Aplikasi disekolah"/>
                                    </div>
                                </Card>
                                <Card className="shadow mt-2">
                                    <h4 className="text-center mt-2">Sedang Online</h4>
                                    <ListGroup>
                                    <ListGroupItem className="text-center"><i className="fas fa-user mr-2"></i> Super Admin</ListGroupItem>
                                    </ListGroup>
                                </Card>
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }
}

export default Dashboard;