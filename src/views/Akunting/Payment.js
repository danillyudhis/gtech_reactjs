import React, { Component } from 'react'
import { Card , Row , Col , Table} from 'reactstrap';
class Payment extends Component {
    render(){
        return(
            <>
            <Row className="mt-2">
                <Col xl="12">
                    <Card>
                        <button className="btn btn-success col-sm-2 m-2">New Payment</button>
                    <Table>
                    <thead>
                        <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Username</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">1</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                        </tr>
                    </tbody>
                    </Table>
                    </Card>
                </Col>
            </Row>
            </>
        );
    }
}

export default Payment;