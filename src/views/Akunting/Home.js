import React, { Component } from 'react';
import { Card , Container, Row , Col , Table} from 'reactstrap';

class Akunting extends Component {
    render() {
        return(
            <>
                <Row className="mt-2">

                    <Col xl="3">
                        <Card className="text-center mt-1">
                            <Container className="my-3">
                                <i className="fas fa-chart-line" style={{fontSize: 45, color: '#99bf2d'}}></i>
                                <h1 style={{fontWeight: 'bold'}}>Rp0</h1>
                                <h4>Total pendapatan</h4>
                            </Container>
                        </Card>
                    </Col>

                    <Col xl="3">
                        <Card className="text-center mt-1">
                            <Container className="my-3">
                                <i className="fas fa-chart-area" style={{fontSize: 45, color: '#dd2979'}}/>
                                <h1 style={{fontWeight: 'bold'}}>Rp250,000</h1>
                                <h4>Total biaya</h4>
                            </Container>
                        </Card>
                    </Col>

                    <Col xl="3">
                        <Card className="text-center mt-1">
                            <Container className="my-3">
                                <i className="fas fa-money-check" style={{fontSize: 45, color: '#f4af08'}}></i>
                                <h1 style={{fontWeight: 'bold'}}>1</h1>
                                <h4>Pending Payment</h4>
                            </Container>
                        </Card>
                    </Col>

                    <Col xl="3">
                        <Card className="text-center mt-1">
                            <Container className="my-3">
                                <i className="far fa-money-bill-alt" style={{fontSize: 45, color: '#0084ff'}}></i>
                                <h1 style={{fontWeight: 'bold'}}>0</h1>
                                <h4>Complete Payment</h4>
                            </Container>
                        </Card>
                    </Col>

                </Row>

                <Row className="mt-1">
                    <Col xl="6">
                        <Card>
                            <Container className="p-2">
                                <h4>Pendapatan Terkini</h4>
                            </Container>
                            <Table striped>
                            <thead>
                                <tr>
                                <th>STATUS</th>
                                <th>SISWA</th>
                                <th>JUDUL</th>
                                <th>AMOUNT</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <th>
                                    <i className="fas fa-circle text-warning"></i> Tertunda
                                </th>
                                <td>Abinaya Bakti</td>
                                <td>Bayaran</td>
                                <td><span class="badge badge-primary">Rp.300000</span></td>
                                </tr>
                            </tbody>
                            </Table>
                        </Card>
                    </Col>
                    <Col xl="6">
                        <Card>
                            <Container className="p-2">
                                <h4>Biaya Terkini</h4>
                            </Container>
                            <Table striped>
                            <thead>
                                <tr>
                                <th>JUDUL</th>
                                <th>KATEGORI</th>
                                <th>AMOUNT</th>
                                <th>METODE</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <th>Spp Agustus</th>
                                <td><span class="badge badge-info">SPP</span></td>
                                <td>Rp250000</td>
                                <td><span class="badge badge-primary">CASH</span></td>
                                </tr>
                            </tbody>
                            </Table>
                        </Card>
                    </Col>
                </Row>
            </>
        );
    }
}

export default Akunting;