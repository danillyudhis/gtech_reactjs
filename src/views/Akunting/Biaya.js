import React, { Component } from 'react';
import { Card , Table} from 'reactstrap';

class Biaya extends Component {
    render() {
        return(
            <>
            <Card className="mt-2">
                <div className="d-flex justify-content-start">
                    <div className="m-2">EXPENSE</div>
                    <div className="m-2">CATEGORI</div>
                </div>
                <Table>
                <thead>
                    <tr>
                    <th>Judul</th>
                    <th>Keterangan</th>
                    <th>Kategori</th>
                    <th>Amount</th>
                    <th>Metode</th>
                    <th>Tanggal</th>
                    <th>Options</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th scope="row">1</th>
                    <td>Mark</td>
                    <td>Otto</td>
                    <td>Otto</td>
                    <td>Otto</td>
                    <td>Otto</td>
                    <td>@mdo</td>
                    </tr>
                </tbody>
                </Table>
            </Card>
            </>
        );
    }
}

export default Biaya;