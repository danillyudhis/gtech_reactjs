import React, { Component } from 'react';
import Header from "components/Headers/Header.js";
import { Route, Switch, Redirect , Link} from "react-router-dom";
import { Card , Container, Row , Col } from 'reactstrap';

import Home from "views/Akunting/Home.js";
import Payment from "views/Akunting/Payment.js";
import Biaya from "views/Akunting/Biaya.js"

class Akunting extends Component {
    render() {
        return(
            <>
            <Header/>
            <Container className="mt--7" fluid>
            <Row>
                <Col xl="6">
                    <Card>

                        <div className="d-flex justify-content-start mt-1">
                            <Link to="/user/akunting">
                            <div className="text-center mt-1 ml-4 mr-5">
                                <i className="fas fa-tachometer-alt fa-2x" />
                                <br/>
                                <h5>Home</h5>
                            </div>
                            </Link>
                            <Link to="/user/akunting/payment">
                            <div className="text-center mt-1 mr-5">
                                <i className="far fa-money-bill-alt fa-2x" />
                                <br/>
                                <h5>Payment</h5>
                            </div>
                            </Link>
                            <Link to="/user/akunting/biaya">
                            <div className="text-center mt-1 mr-5">
                                <i className="fas fa-coins fa-2x" />
                                <br/>
                                <h5>Home</h5>
                            </div>
                            </Link>
                        </div>

                    </Card>
                </Col>
            </Row>
            <Switch>
                <Route path="/user/akunting" exact={true} component={Home} key={1} />
                <Route path="/user/akunting/payment" exact={true} component={Payment} key={2} />
                <Route path="/user/akunting/biaya" exact={true} component={Biaya} key={3} />
                <Redirect from="*" to="/user/akunting" />
            </Switch>
            </Container>
            </>
        );
    }
}

export default Akunting;