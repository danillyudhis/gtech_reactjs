import React, { Component } from 'react';
import { Container, Row, Col, Card } from 'reactstrap';
import Header from "components/Headers/Header.js";
class index extends Component {
    render() {
        return(
            <>
            <Header/>
            <Container className="mt--7" fluid>
            <h1 style={{color: 'black'}}>Berita</h1>
                <Row>
                    <Col xl="6">
                        <Card>
                            <Row className="mt-2">
                                <Col xl="1" className="text-center ml-3">
                                <i className="fas fa-user"></i>
                                </Col>
                                <Col xl="3">
                                    <h5>Super Admin</h5>
                                    <h5>11, aug 10:20 AM</h5>
                                </Col>
                            </Row>
                            <hr />
                            <div className="ml-2 mb-2">Proudly Present .. coming soon</div>
                            <img src="https://new.disekolah.id/uploads/news_images/5aa7292345.jpg" alt="empty" />
                        </Card>
                    </Col>
                </Row>
            </Container>
            </>
        );
    }
}

export default index;