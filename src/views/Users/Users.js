import React, { Component } from 'react';

import {
    Card,
    Container,
    Row,
    Col
  } from "reactstrap";

  import Header from "components/Headers/Header.js";

class Users extends Component {
    render() {
        return(
            <>
            <Header />
            <Container className="mt--7" fluid>
                <Row>
                    <Col xl="3" className="mt-1">
                        <Card className="d-flex justify-content-center">
                            <div className="d-flex justify-content-center mt-4">
                                <div>
                                    <img src="https://new.disekolah.id/uploads/icons/admins.svg" alt="empty" style={{width: "100px"}}/> 
                                </div>
                            </div>
                            <div className="d-flex justify-content-center mt-2">
                                    <h5>  Admins </h5>
                            </div>
                            <div className="d-flex justify-content-center ">
                                    <h5> 2 Admins </h5>
                            </div>
                        </Card>
                    </Col>
                    <Col xl="3" className="mt-1">
                        <Card className="d-flex justify-content-center">
                            <div className="d-flex justify-content-center mt-4">
                                <div>
                                    <img src="https://new.disekolah.id/uploads/icons/teachers.svg" alt="empty" style={{width: "100px"}}/> 
                                </div>
                            </div>
                            <div className="d-flex justify-content-center mt-2">
                                    <h5>  Guru </h5>
                            </div>
                            <div className="d-flex justify-content-center ">
                                    <h5> 12 Guru </h5>
                            </div>
                        </Card>
                    </Col>
                    <Col xl="3" className="mt-1">
                        <Card className="d-flex justify-content-center">
                            <div className="d-flex justify-content-center mt-4">
                                <div>
                                    <img src="https://new.disekolah.id/uploads/icons/students.svg" alt="empty" style={{width: "100px"}}/> 
                                </div>
                            </div>
                            <div className="d-flex justify-content-center mt-2">
                                    <h5>  Siswa </h5>
                            </div>
                            <div className="d-flex justify-content-center ">
                                    <h5> 63 Siswa </h5>
                            </div>
                        </Card>
                    </Col>
                    <Col xl="3" className="mt-1">
                        <Card className="d-flex justify-content-center">
                            <div className="d-flex justify-content-center mt-4">
                                <div>
                                    <img src="https://new.disekolah.id/uploads/icons/parents.svg" alt="empty" style={{width: "100px"}}/> 
                                </div>
                            </div>
                            <div className="d-flex justify-content-center mt-2">
                                    <h5>  Orang Tua </h5>
                            </div>
                            <div className="d-flex justify-content-center ">
                                    <h5> 2 Orang Tua </h5>
                            </div>
                        </Card>
                    </Col>
                    <Col xl="3" className="mt-1">
                        <Card className="d-flex justify-content-center">
                            <div className="d-flex justify-content-center mt-4">
                                <div>
                                    <img src="https://new.disekolah.id/uploads/icons/accountant.svg" alt="empty" style={{width: "100px"}}/> 
                                </div>
                            </div>
                            <div className="d-flex justify-content-center mt-2">
                                    <h5>  Keuangan </h5>
                            </div>
                            <div className="d-flex justify-content-center ">
                                    <h5> 0 Keuangan </h5>
                            </div>
                        </Card>
                    </Col>
                    <Col xl="3" className="mt-1">
                        <Card className="d-flex justify-content-center">
                            <div className="d-flex justify-content-center mt-4">
                                <div>
                                    <img src="https://new.disekolah.id/uploads/icons/librarian.svg" alt="empty" style={{width: "100px"}}/> 
                                </div>
                            </div>
                            <div className="d-flex justify-content-center mt-2">
                                    <h5>  Perpustaaan </h5>
                            </div>
                            <div className="d-flex justify-content-center ">
                                    <h5> 12 buku </h5>
                            </div>
                        </Card>
                    </Col>
                    <Col xl="3" className="mt-1">
                        <Card className="d-flex justify-content-center">
                            <div className="d-flex justify-content-center mt-4">
                                <div>
                                    <img src="https://new.disekolah.id/uploads/icons/pendings.svg" alt="empty" style={{width: "100px"}}/> 
                                </div>
                            </div>
                            <div className="d-flex justify-content-center mt-2">
                                    <h5>  Pending Users </h5>
                            </div>
                            <div className="d-flex justify-content-center ">
                                    <h5> 0 Tertunda </h5>
                            </div>
                        </Card>
                    </Col>
                </Row>
            </Container>
            </>
        );
    }
}

export default Users;