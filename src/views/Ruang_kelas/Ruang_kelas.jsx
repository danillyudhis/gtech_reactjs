import React, { useState } from 'react';
import { Input, Row, Col, Container, Card, DropdownMenu,
         DropdownItem,
         UncontrolledDropdown,
         DropdownToggle, 
         Media, Button, Modal, ModalHeader, ModalBody, ModalFooter , FormGroup , Table } from 'reactstrap';
import Header from "components/Headers/Header.js";

const Ruang_kelas = (props) => {
    const {
      className
    } = props;

    const [modal, setModal] = useState(false);
    
    const toggle = () => setModal(!modal);

    const [section ,Setsection] = useState(false);

    function section_w(val){
        toggle();
        Setsection(section => section = val);
    }

    function Table_siswa() {
        return (
            <Table>
            <thead>
                <tr>
                <th>#</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Username</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <th scope="row">1</th>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
                </tr>
                <tr>
                <th scope="row">2</th>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
                </tr>
                <tr>
                <th scope="row">3</th>
                <td>Larry</td>
                <td>the Bird</td>
                <td>@twitter</td>
                </tr>
            </tbody>
            </Table>
        );
    }

    function Edit_field() {
        return (
            <Input type="text" placeholder="Nama Kelas" />
        );
    }

    return(
        <>
        <Header/>
        <Container className="mt--7" fluid>
            <Row>

                <Col xl="6">
                    <Card>
                        <div className="container m-1">
                                <div className="d-flex justify-content-start">
                                        <div className="border border-primary rounded-circle text-center d-flex justify-content-center" style={{width: '75px', height: '75px'}}>
                                            <span style={{fontSize: '48px'}}>P</span>
                                        </div>
                                        <div className="ml-2">
                                            <h4>Kelas A8</h4>
                                            <h4>Siswa: 1</h4>
                                        </div>
                                </div>
                                <div className="d-flex justify-content-end">
                                    <UncontrolledDropdown nav className="align-right">
                                            <DropdownToggle className="pr-0" nav>
                                            <Media className="align-items-center">
                                                <Media className="ml-2 d-none d-lg-block">
                                                <span className="mb-0 text-sm font-weight-bold">
                                                    ....
                                                </span>
                                                </Media>
                                            </Media>
                                            </DropdownToggle>
                                            <DropdownMenu className="dropdown-menu-arrow" right>
                                            <DropdownItem onClick={() => section_w("edit") }>
                                                <i className="ni ni-settings-gear-65" />
                                                <span>Edit</span>
                                            </DropdownItem>
                                            <DropdownItem onClick={() => section_w("siswa") }>
                                                <i className="ni ni-single-02" />
                                                <span>Siswa</span>
                                            </DropdownItem>
                                            </DropdownMenu>
                                    </UncontrolledDropdown>
                                </div>
                        </div>
                    </Card>
                </Col>

                <Col xl="6">
                    <Card>
                        <div className="container m-1">
                                <div className="d-flex justify-content-start">
                                        <div className="border border-primary rounded-circle text-center d-flex justify-content-center" style={{width: '75px', height: '75px'}}>
                                            <span style={{fontSize: '48px'}}>P</span>
                                        </div>
                                        <div className="ml-2">
                                            <h4>Private Class</h4>
                                            <h4>Siswa: 1</h4>
                                        </div>
                                </div>
                                <div className="d-flex justify-content-end">
                                    <UncontrolledDropdown nav className="align-right">
                                            <DropdownToggle className="pr-0" nav>
                                            <Media className="align-items-center">
                                                <Media className="ml-2 d-none d-lg-block">
                                                <span className="mb-0 text-sm font-weight-bold">
                                                    ....
                                                </span>
                                                </Media>
                                            </Media>
                                            </DropdownToggle>
                                            <DropdownMenu className="dropdown-menu-arrow" right>
                                            <DropdownItem onClick={() => section_w("edit") }>
                                                <i className="ni ni-settings-gear-65" />
                                                <span>Edit</span>
                                            </DropdownItem>
                                            <DropdownItem onClick={() => section_w("siswa") }>
                                                <i className="ni ni-single-02" />
                                                <span>Siswa</span>
                                            </DropdownItem>
                                            </DropdownMenu>
                                    </UncontrolledDropdown>
                                </div>
                        </div>
                    </Card>
                </Col>

            </Row>
        </Container>

        <Modal isOpen={modal} toggle={toggle} className={className}>
            <ModalHeader toggle={toggle}>{section === "edit"? "Update" : "Siswa"}</ModalHeader>
            <ModalBody>
            <FormGroup>
                {section === "edit" ? <Edit_field/> : <Table_siswa/>}
            </FormGroup>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={toggle}>Do Something</Button>{' '}
              <Button color="secondary" onClick={toggle}>Cancel</Button>
            </ModalFooter>
         </Modal>
        </>
    );
}


export default Ruang_kelas;