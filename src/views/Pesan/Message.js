import React, { Component } from 'react';
import { FormGroup, Label, Input, Container, FormText } from 'reactstrap';

class Message extends Component {
    render(){
        return(
            <Container>
            <FormGroup>
                <Label>Penerima</Label>
                <Input type="select" name="select" id="exampleSelect">
                <option>Pilih</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                </Input>
            </FormGroup>
            <div style={{width: '500rem', paddingBottom: '20rem'}}>
            </div>
            <FormGroup>
                <Label>Ketik Pesan Anda</Label>
                <Input type="text" name="text"/>
            </FormGroup>
            <FormGroup>
                <Label>File</Label>
                <Input type="file" name="file"/>
                <button className="btn btn-success mt-5">Kirim</button>
            </FormGroup>
            </Container>
        );
    }
}

export default Message;