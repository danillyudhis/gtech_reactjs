import React, { Component } from 'react';

import {  NavLink } from "react-router-dom";

class Group extends Component {
    render() {
        return(
            <>
            <div className="d-flex justify-content-center">
                <img src="https://new.disekolah.id/uploads/mensajeseducaby.svg" width="400" height="400" className="mt-7" alt="Aplikasi disekolah"/>
                <br/>
            </div>
            <div className="d-flex justify-content-center mb-5">
                <div className="row">
                    <div className="col-sm-3">
                        <NavLink to="/user/pesan/bikin_group" className="btn btn-success">Bikin Group</NavLink>
                    </div>
                </div>
            </div>
            </>
        );
    }
}

export default Group;