import React, { Component } from 'react';
import { FormGroup, Label, Input, Container, Table , CustomInput } from 'reactstrap';

class Bikin_group extends Component {
    constructor(props) {
        super(props)
        this.state = {}
    }
    
    chk_teach() {    
    }

    render(){
        return(
            <Container>
            <h1>Create New Group</h1>
            <FormGroup>
                <Label>Nama</Label>
                <Input type="text"/>
            </FormGroup>
            <FormGroup>
            <Label className="mt-2 mb-2">Student</Label>
            <Table>
                <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                    <th>Pilih</th>
                    <th>User</th>
                    <th>Nama</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th> <CustomInput type="checkbox" id="exampleCustomCheckbox1"/> </th>
                    <td>ssmpsiswa7a04</td>
                    <td>Abinaya Bakti</td>
                    </tr>
                    <tr>
                    <th> <CustomInput type="checkbox" id="exampleCustomCheckbox2"/> </th>
                    <td>ssmpsiswa7a05</td>
                    <td>Abyasa Bamantara</td>
                    </tr>
                    <tr>
                    <th> <CustomInput type="checkbox" id="exampleCustomCheckbox3"/> </th>
                    <td>ssmpsiswa7a01</td>
                    <td>Abichandra Bagas</td>
                    </tr>
                </tbody>
                </Table>
            <Label className="mt-2 mb-2">Teacher</Label>
            <Table>
                <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                    <th>Pilih</th>
                    <th>User</th>
                    <th>Nama</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th> <CustomInput type="checkbox" id="exampleCustomCheckbox5" className="teacher_chk"/> </th>
                    <td> Guru2</td>
                    <td> Udin Saefulloh S.Kom </td>
                    </tr>
                    <tr>
                    <th> <CustomInput type="checkbox" id="exampleCustomCheckbox6" className="teacher_chk"/> </th>
                    <td> guru3 </td>
                    <td> Teti Kurnaeti, S.Pd  </td>
                    </tr>
                </tbody>
                </Table>
            <Label className="mt-2 mb-2">Parent</Label>
            <Table>
                <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                    <th>Pilih</th>
                    <th>User</th>
                    <th>Nama</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th> <CustomInput type="checkbox" id="exampleCustomCheckbox5" className="teacher_chk"/> </th>
                    <td> ortu1 </td>
                    <td> Sukma Gunawan </td>
                    </tr>
                    <tr>
                    <th> <CustomInput type="checkbox" id="exampleCustomCheckbox6" className="teacher_chk"/> </th>
                    <td> ortu-2 </td>
                    <td> Ahmad Zulkarnaen  </td>
                    </tr>
                </tbody>
                </Table>
            <Label className="mt-2 mb-2">Admin</Label>
            <Table>
                <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                    <th>Pilih</th>
                    <th>User</th>
                    <th>Nama</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th> <CustomInput type="checkbox" id="exampleCustomCheckbox5" className="teacher_chk"/> </th>
                    <td> admin </td>
                    <td> Super Admin </td>
                    </tr>
                    <tr>
                    <th> <CustomInput type="checkbox" id="exampleCustomCheckbox6" className="teacher_chk"/> </th>
                    <td> admin-1 </td>
                    <td> admin- 1  </td>
                    </tr>
                </tbody>
            </Table>
            <Label className="mt-2 mb-2">Accountant</Label>
            <Table>
                <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                    <th>Pilih</th>
                    <th>User</th>
                    <th>Nama</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </Table>
            <Label className="mt-2 mb-2">Librarian</Label>
            <Table>
                <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                    <th>Pilih</th>
                    <th>User</th>
                    <th>Nama</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </Table>
            </FormGroup>
            <button className="btn btn-success align-right mb-2 mt-1">Bikin Grup</button>
            </Container>
        );
    }
}

export default Bikin_group;