import React, { Component } from 'react';
import { Route, Switch, Redirect , NavLink } from "react-router-dom";

import {
    Button,
    Card,
    NavItem,
    Nav,
    Container,
    Row,
    Col,
    Navbar
  } from "reactstrap";

  import Header from "components/Headers/Header.js";

import Chats from "./Chats";
import Message from "./Message";
import Group from "./Group";
import Bikin_group from "./Bikin_group";
class Pesan extends Component {
    render(){
        return(
            <>
            <Header/>
            <Container className="mt--7" fluid>
                <Row>

                    <Col className="col col-xl-4 order-xl-4 col-lg-4 order-lg-4 col-md-4 col-sm-4 col-4">
                        <Card className="shadow mt-2">    
                            <Navbar expand="md" className="d-flex justify-content-center" style={{background: 'white'}}>
                                <Nav className="d-flex justify-content-between" navbar>
                                    <NavLink exact to="/user/pesan" activeClassName="text-warning">
                                        <NavItem className="navs-item text-center">
                                            <i className="far fa-envelope text-center"></i>
                                            <br/>
                                            <span>CHATS</span>
                                        </NavItem>
                                    </NavLink>
                                
                                    <NavLink exact to="/user/pesan/tulist_mess" activeClassName="text-warning">
                                        <NavItem className="navs-item text-center ml-4 mr-4">
                                            <i className="fas fa-pen-square"></i>
                                            <br/>
                                            <span>TULIS</span>
                                        </NavItem>
                                    </NavLink>
                                
                                    <NavLink exact to="/user/pesan/group" activeClassName="text-warning">
                                    <NavItem className="navs-item text-center">
                                        <i className="fas fa-users"></i>
                                        <br/>
                                        <span>GROUP</span>
                                    </NavItem>
                                    </NavLink>
                                    
                                </Nav>
                            </Navbar>
                            <div style={{marginBottom: '100px'}}/>
                        </Card>
                    </Col>

                    <Col className="col col-xl-8 order-xl-8 col-lg-8 order-lg-8 col-md-8 col-sm-8 col-8">
                        <Card className="shadow mt-2 d-flex justify-content-center mb-5">
                            <Switch>
                                <Route path="/user/pesan" exact={true} component={Chats} key={1} />
                                <Route path="/user/pesan/tulist_mess" exact={true} component={Message} key={2} />
                                <Route path="/user/pesan/group" exact={true} component={Group} key={3}/>
                                <Route path="/user/pesan/bikin_group" exact={true} component={Bikin_group} key={4}/>
                                <Redirect from="*" to="/user/pesan" />
                            </Switch>
                        </Card>
                    </Col>

                </Row>
            </Container>
            </>
        );
    }
}

export default Pesan;