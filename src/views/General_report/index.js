import React, { Component } from 'react';
import Header from "components/Headers/Header.js";
import { Card , Container, Row , Col } from 'reactstrap';
import { Route, Switch, Redirect , Link } from "react-router-dom";

import Classes from "./Classes";
import Student from "./Student";
import Kehadiran from "./Kehadiran";
import Mark from "./Mark";
import Tabulation from "./Tabulation";
import Accounting from "./Accounting";

class index extends Component {
    render() {
        return(
            <>
            <Header/>
            <Container className="mt--7" fluid>
                <Row>
                    <Col xl="12">
                        <Card>
                            <div className="d-flex justify-content-start mt-1">
                                <Link to="/user/system_report">
                                <div className="text-center mt-1 ml-4 mr-5">
                                    <i className="fas fa-trophy" />
                                    <br/>
                                    <h5>Classes</h5>
                                </div>
                                </Link>
                                <Link to="/user/system_report/student_report">
                                <div className="text-center mt-1 mr-5">
                                    <i className="fas fa-user-graduate" />
                                    <br/>
                                    <h5>Siswa</h5>
                                </div>
                                </Link>
                                <Link to="/user/system_report/kehadiran">
                                <div className="text-center mt-1 mr-5">
                                    <i className="far fa-calendar-alt" />
                                    <br/>
                                    <h5>Kehadiran</h5>
                                </div>
                                </Link>
                                <Link to="/user/system_report/mark">
                                <div className="text-center mt-1 mr-5">
                                    <i className="fas fa-clipboard-check" />
                                    <br/>
                                    <h5>Final Mark</h5>
                                </div>
                                </Link>
                                <Link to="/user/system_report/tabulation_sheet">
                                <div className="text-center mt-1 mr-5">
                                    <i className="fas fa-scroll" />
                                    <br/>
                                    <h5>Tabulation Sheet</h5>
                                </div>
                                </Link>
                                <Link to="/user/system_report/accounting_report">
                                <div className="text-center mt-1 mr-5">
                                    <i className="fas fa-money-bill-wave" />
                                    <br/>
                                    <h5>Akunting</h5>
                                </div>
                                </Link>
                            </div>
                            <div className="container">
                                <Switch>
                                    <Route path="/user/system_report" exact={true} component={Classes} key={1} />
                                    <Route path="/user/system_report/student_report" exact={true} component={Student} key={2} />
                                    <Route path="/user/system_report/kehadiran" exact={true} component={Kehadiran} key={3} />
                                    <Route path="/user/system_report/mark" exact={true} component={Mark} key={4} />
                                    <Route path="/user/system_report/tabulation_sheet" exact={true} component={Tabulation} key={4} />
                                    <Route path="/user/system_report/accounting_report" exact={true} component={Accounting} key={4} />
                                    <Redirect from="*" to="user/system_report" />
                                </Switch>
                            </div>
                        </Card>
                    </Col>
                </Row>
            </Container>
            </>
        );
    }
}

export default index;