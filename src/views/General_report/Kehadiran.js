import React, { Component } from 'react';
import { Input, Row, Col, Label } from 'reactstrap';

class Kehadiran extends Component {
    render() {
        return (
            <>
                <div className="m-3">
                    <Row>
                        <Col xl='3'>
                            <Label>Tingkat</Label>
                            <Input type="select" name="select">
                                <option>Pilih</option>
                                <option>Tingkat VII</option>
                                <option>Tingkat VIII</option>
                                <option>Tingkat IX</option>
                                <option>Private</option>
                            </Input>
                        </Col>
                        <Col xl='3'>
                            <Label>Kelas</Label>
                            <Input type="select" name="select">
                                <option>Pilih</option>
                                <option>VIII-A</option>
                                <option>VIII-B</option>
                            </Input>
                        </Col>
                        <Col xl="3">
                            <Label>Bulan</Label>
                            <Input type="select" name="select">
                                <option>Pilih</option>
                                <option>Januari</option>
                                <option>Februari</option>
                                <option>Maret</option>
                            </Input>
                        </Col>
                        <Col xl='2'>
                            <Label><span style={{ color: 'white' }}>hidedsdsdsadsadad</span></Label>
                            <button className="btn btn-success">Get report</button>
                        </Col>
                    </Row>
                    <div className="text-center mt-5 mb-5">
                        <h3>Tingkat VIII - Kelas : VIII - B</h3>
                        <h4>Tahun: 2020</h4>
                    </div>

                </div>
            </>
        );
    }
}

export default Kehadiran;