import React, { Component } from 'react'
import { Input , Row, Col , Label, Table} from 'reactstrap';

class Classes extends Component {
    render(){
        return(
            <>
            <div className="m-3">
                <h4>CLASS REPORT</h4>
                <Row>
                    <Col xl='3'>
                        <Label>Tingkat</Label>
                        <Input type="select" name="select">
                        <option>Pilih</option>
                        <option>Tingkat VII</option>
                        <option>Tingkat VIII</option>
                        <option>Tingkat IX</option>
                        <option>Private</option>
                        </Input>
                    </Col>
                    <Col xl='3'>
                        <Label>Kelas</Label>
                        <Input type="select" name="select">
                        <option>Pilih</option>
                        <option>VIII-A</option>
                        <option>VIII-B</option>
                        </Input>
                    </Col>
                    <Col xl='2'>
                        <Label><span style={{color: 'white'}}>hidedsdsdsadsadad</span></Label>
                        <button className="btn btn-success">Get report</button>
                    </Col>
                </Row>
                <Row className="mt-3">
                    <Col xl="6">
                        <div className="text-center">
                            <h3>Tingkat VIII - Kelas: VIII - A</h3>
                            <h4>Guru: Lily Kamaliah, S,Pd.I</h4>
                            <h4>10 Siswa | 10 Mata Pelajaran.</h4>
                            <h4>Running year: 2020</h4>
                        </div>
                    </Col>
                    <Col xl="6">
                        <h3>Mata Pelajaran</h3>
                        <Table>
                        <thead>
                            <tr>
                            <th>Subject</th>
                            <th>Guru</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td>Bahasa Inggris</td>
                            <td><span className="badge badge-info" style={{color: 'black'}}>DIAH ARWULAN</span></td>
                            </tr>
                            <tr>
                            <td>Bahasa Indonesia</td>
                            <td><span className="badge badge-info" style={{color: 'black'}}>IZHATULLAH</span></td>
                            </tr>
                            <tr>
                            <td>Seni Budaya</td>
                            <td><span className="badge badge-info" style={{color: 'black'}}>MISNAIN</span></td>
                            </tr>
                        </tbody>
                        </Table>
                    </Col>
                </Row>
            </div>
            </>
        );
    }
}

export default Classes;