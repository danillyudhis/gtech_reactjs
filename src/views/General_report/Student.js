import React, { Component } from 'react';
import { Input , Row, Col , Label, Table} from 'reactstrap';

class Student extends Component {
    render(){
        return(
            <>
                <h4>STUDENT REPORT</h4>
                <Row>
                    <Col xl='3'>
                        <Label>Tingkat</Label>
                        <Input type="select" name="select">
                        <option>Pilih</option>
                        <option>Tingkat VII</option>
                        <option>Tingkat VIII</option>
                        <option>Tingkat IX</option>
                        <option>Private</option>
                        </Input>
                    </Col>
                    <Col xl='3'>
                        <Label>Kelas</Label>
                        <Input type="select" name="select">
                        <option>Pilih</option>
                        <option>VIII-A</option>
                        <option>VIII-B</option>
                        </Input>
                    </Col>
                    <Col xl='2'>
                        <Label><span style={{color: 'white'}}>hidedsdsdsadsadad</span></Label>
                        <button className="btn btn-success">Get report</button>
                    </Col>
                </Row>
                <div className="text-center mt-5">
                    <h3>Tingkat VII - Kelas: VII - A</h3>
                    <h5>Guru: Udin Saefulloh S.Kom</h5>
                    <h5>12 Siswa | 10 Mata Pelajaran.</h5>
                    <h5>Running year: 2020</h5>
                </div>
                <Row className="mt-3">
                    <Col xl="4">

                    </Col>
                    <Col xl="8">
                        <Table>
                        <thead>
                            <tr>
                            <th>Nama</th>
                            <th>ROLL ID</th>
                            <th>ORANG TUA</th>
                            <th>STATUS</th>
                            <th>KELAMIN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <td>Abinaya Bakti</td>
                            <td>12345673</td>
                            <td>Sukma Gunawan</td>
                            <td className="text-center"><span className="badge badge-info" style={{color: 'black'}}>Aktif</span></td>
                            <td className="text-center"><span className="badge badge-success" style={{color: 'black'}}>L</span></td>
                            </tr>
                            <tr>
                            <td>Abyasa</td>
                            <td>12345673</td>
                            <td>Sukma Gunawan</td>
                            <td className="text-center"><span className="badge badge-info" style={{color: 'black'}}>Aktif</span></td>
                            <td className="text-center"><span className="badge badge-success" style={{color: 'black'}}>L</span></td>
                            </tr>
                        </tbody>
                        </Table>
                    </Col>
                </Row>
            </>
        );
    }
}

export default Student;