import React, { Component } from 'react';
import { Input, Row, Col, Label, Table } from 'reactstrap';

class Tabulation extends Component {
    render(){
        return(
            <>
                <Row>
                    <Col xl='2'>
                        <Label>Tingkat</Label>
                        <Input type="select" name="select">
                        <option>Pilih</option>
                        <option>Tingkat VII</option>
                        <option>Tingkat VIII</option>
                        <option>Tingkat IX</option>
                        <option>Private</option>
                        </Input>
                    </Col>
                    <Col xl='2'>
                        <Label>Kelas</Label>
                        <Input type="select" name="select">
                        <option>Pilih</option>
                        <option>VIII-A</option>
                        <option>VIII-B</option>
                        </Input>
                    </Col>
                    <Col xl='2'>
                        <Label>Subject</Label>
                        <Input type="select" name="select">
                        <option>Pilih</option>
                        <option>Bahasa Indonesia</option>
                        <option>Bahasa Inggris</option>
                        <option>Seni Budaya</option>
                        <option>Tik</option>
                        </Input>
                    </Col>
                    <Col xl='2'>
                        <Label><span style={{color: 'white'}}>hidedsdsdsadsadad</span></Label>
                        <button className="btn btn-success">Get report</button>
                    </Col>
                </Row>
                
                <div className="row justify-content-center mt-5" style={{marginLeft: '8rem'}}>
                    <div className="col-auto">
                        <img src="https://new.disekolah.id/uploads/3dcb5f9b376fcfe6f92fd1ebc205c7eeicon-logo-cyrcle.png" alt="empy" width="40%" />
                        <p>Aplikasi disekolah</p>
                        <p className="mt--4">SMP Digital School</p>
                        <p className="mt--4">Tabulation sheet</p>
                        <p className="mt--4">Bahasa Indonesia</p>
                        <p className="mt--4">Tingkat VII | VII - A</p>
                    </div>
                </div>

                <Table>
                <thead>
                    <tr>
                    <th>#</th>
                    <th>Kelamin</th>
                    <th>Siswa</th>
                    <th>Semester Ganjil 2020/2021</th>
                    <th>Semester Genap 2020/2021</th>
                    <th>Average</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th scope="row">1</th>
                    <td>M</td>
                    <td>Abinaya Bakti</td>
                    <td></td>
                    <td></td>
                    <td>0</td>
                    </tr>
                    <tr>
                    <th scope="row">2</th>
                    <td>M</td>
                    <td>Abyasa Bamantara</td>
                    <td></td>
                    <td></td>
                    <td>0</td>
                    </tr>
                    <tr>
                    <th scope="row">3</th>
                    <td>M</td>
                    <td>Abichandra Bagas</td>
                    <td></td>
                    <td></td>
                    <td>0</td>
                    </tr>
                    <tr>
                    <th scope="row">4</th>
                    <td>M</td>
                    <td>Abimana Bagaskoro</td>
                    <td></td>
                    <td></td>
                    <td>0</td>
                    </tr>
                </tbody>
                </Table>
                <Row>
                    <Col xl="2">
                        <span>Mens <span className="ml-3">3</span></span>
                    </Col>
                    <Col xl="2">
                        <span>Women <span className="ml-3">0</span></span>
                    </Col>
                </Row>
                <p>Guru : </p>
                <p>Signature : </p>
            </>
        );
    }
}

export default Tabulation;