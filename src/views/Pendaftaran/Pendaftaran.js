import React from 'react';
import { Card, Container } from "reactstrap";
import { Route, Switch, Redirect , NavLink , Con} from "react-router-dom";
import Header from "components/Headers/Header.js";

import Info_siswa from "views/Pendaftaran/Info_siswa";
import Wali_siswa from "./Wali_siwa";
import Pelengkap from "./Pelengkap";
const Pendaftaran = (props) => {

  return (
    <div>
        <Header/>
            <Container className="mt--7" fluid>
                <Card>
                <div className="d-flex justify-content-around mt-1">
                    <NavLink to="/user/pendaftaran" exact={true} activeClassName="text-warning">
                    <div className="text-center mt-1 ml-4 mr-5">
                        <h3>Info Siswa</h3>
                    </div>
                    </NavLink>
                    <NavLink to="/user/pendaftaran/wali_siswa" exact={true} activeClassName="text-warning">
                    <div className="text-center mt-1 mr-5">
                        <h3>Info Wali Siswa</h3>
                    </div>
                    </NavLink>
                    <NavLink to="/user/pendaftaran/pelengkap" exact={true} activeClassName="text-warning">
                    <div className="text-center mt-1 mr-5">
                        <h3>Data Pelengkap</h3>
                    </div>
                    </NavLink>
                </div>
                <div>
                    <Container>
                        <Switch>
                            <Route path="/user/pendaftaran" exact={true} component={Info_siswa} key={1} />
                            <Route path="/user/pendaftaran/wali_siswa" exact={true} component={Wali_siswa} key={2} />
                            <Route path="/user/pendaftaran/pelengkap" exact={true} component={Pelengkap} key={3} />
                            <Redirect from="*" to="/user/pendaftaran" />
                        </Switch>
                    </Container>
                </div>
                </Card>
            </Container>
    </div>
  );
}

export default Pendaftaran;