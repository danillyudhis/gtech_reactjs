import React, { Component } from 'react';
import { Row , Col , FormGroup, Label, Input } from "reactstrap";

class Pelengkap extends Component {
    render() {
        return(
            <>
            <Row>
                <Col xl="6">
                    <FormGroup>
                        <Label>Kelainan atau Penyakit</Label>
                        <Input type="text"/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Dokter Pribadi</Label>
                        <Input type="text"/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Yang Berwewenang</Label>
                        <Input type="text"/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Catatan</Label>
                        <Input type="textarea"/>
                    </FormGroup>
                </Col>
                <Col xl="6">
                    <FormGroup>
                        <Label>Alergi</Label>
                        <Input type="text"/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Nomor Dokter</Label>
                        <Input type="number"/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Nomor Yg Berwewenang</Label>
                        <Input type="number"/>
                    </FormGroup>
                </Col>
            </Row>
            <button className="btn btn-success mt-2 mb-2">Daftar</button>
            </>
        );
    }
}

export default Pelengkap;