import React, { Component } from 'react';
import { Row , Col , FormGroup, Label, Input } from "reactstrap";

class Info_siswa extends Component {
    render(){
        return(
            <>
            <Row>
                <Col xl="6">
                    <FormGroup>
                        <Label>Nama Depan</Label>
                        <Input type="text"/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Nama Depan</Label>
                        <Input type="date"/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Phone</Label>
                        <Input type="number"/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Username</Label>
                        <Input type="text"/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Alamat</Label>
                        <Input type="text"/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Tingkat</Label>
                        <Input type="select" name="select">
                        <option>Pilih</option>
                        </Input>
                    </FormGroup>
                    <FormGroup>
                        <Label>School Bus</Label>
                        <Input type="select" name="select">
                        <option>Pilih</option>
                        </Input>
                    </FormGroup>
                </Col>
                <Col xl="6">
                    <FormGroup>
                        <Label>Nama Belakang</Label>
                        <Input type="text"/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Email</Label>
                        <Input type="email"/>
                    </FormGroup>
                    <FormGroup>
                        <Label>Kelamin</Label>
                        <Input type="select" name="select">
                        <option>Pilih</option>
                        </Input>
                    </FormGroup>
                </Col>
            </Row>
            <button className="btn btn-success align-right mb-2">Berikutnya</button>
            </>
        );
    }
}

export default Info_siswa;