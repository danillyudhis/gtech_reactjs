import React, {Component} from 'react';
import { Button, Form, FormGroup, Label, Input, FormText , Row, Col, CustomInput } from 'reactstrap';
import BootstrapSwitchButton from 'bootstrap-switch-button-react'

class Wali_siswa extends Component {
    
    render() {
        return(
            <>
            <Row>
                <Col xl="6">
                <div className="row justify-content-start">
                <div className="col-10">
                    <p>Pilih Orangtua</p>
                    <p>Jika Anda ingin menambahkan orang tua baru, tandai opsi ini</p>
                </div>
                <div className="col-2">
                    <div className="mt-4">
                        <BootstrapSwitchButton
                            checked={false}
                            onlabel='Yes'
                            onstyle='danger'
                            offlabel='No'
                            offstyle='success'
                            style='w-100 mx-3 pb-4'
                            onChange={(checked: boolean) => {
                                this.setState({ isUserAdmin: checked })
                            }}
                        />
                    </div>
                </div>
                </div>
                </Col>
                <Col xl="6">
                    <FormGroup>
                        <Label>Pilih Orang Tua</Label>
                        <Input type="select" name="select" id="exampleSelect">
                        <option>Pilih</option>
                        <option>Sukma Gunawan</option>
                        </Input>
                    </FormGroup>
                </Col>
            </Row>
            <button className="btn btn-success align-right mb-2">Berikutnya</button>
            </>
        );
    }
}

export default Wali_siswa;