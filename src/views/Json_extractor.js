export default (val) => {
    if(val == undefined || val == false) {
        return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGVja2luZ191c2VyIjpmYWxzZSwiaWF0IjoxNjAwMzkzMDE2fQ.RmaIEqM81VVcivTu25JRt0hdvrsv-HtKh4_wp_mIKXQ";
    } else {
        return val;
    }
}